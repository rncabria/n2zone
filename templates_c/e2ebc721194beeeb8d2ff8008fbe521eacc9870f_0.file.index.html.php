<?php /* Smarty version 3.1.27, created on 2015-10-13 02:10:03
         compiled from "/Applications/mamp/apache2/htdocs/n2zone/templates/modules/users/index.html" */ ?>
<?php
/*%%SmartyHeaderCode:1232418019561cca6b0a11e5_87376998%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e2ebc721194beeeb8d2ff8008fbe521eacc9870f' => 
    array (
      0 => '/Applications/mamp/apache2/htdocs/n2zone/templates/modules/users/index.html',
      1 => 1440354019,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1232418019561cca6b0a11e5_87376998',
  'variables' => 
  array (
    'filter' => 0,
    'ddl_status' => 0,
    'key' => 0,
    'item' => 0,
    '_base' => 0,
    '_module' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561cca6b155467_98235209',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561cca6b155467_98235209')) {
function content_561cca6b155467_98235209 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1232418019561cca6b0a11e5_87376998';
?>
<div class="row">
	<div class="col-lg-12">
	
		<div class="panel panel-info">
			<div class="panel-heading">
			Users
			<?php if ($_smarty_tpl->tpl_vars['filter']->value['id_user_group']) {?>
			&nbsp;<i class="fa fa-angle-right"></i>&nbsp;<?php if ($_smarty_tpl->tpl_vars['filter']->value['id_user_group'] == 1) {?>Administrators<?php } elseif ($_smarty_tpl->tpl_vars['filter']->value['id_user_group'] == 2) {?>Registered Users<?php }?>
			<?php }?>

					<form role="form" method="post" action="" class="pull-right">
						<select name="filter[status]" onchange="this.form.submit();">
							<option value=""></option>
							<?php
$_from = $_smarty_tpl->tpl_vars['ddl_status']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value == $_smarty_tpl->tpl_vars['filter']->value['status']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</option>
							<?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
							
						</select>
					</form>			
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-2">
						<a href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;
echo $_smarty_tpl->tpl_vars['_module']->value;?>
/add" class="btn btn-success btn-w-m"><i class="fa fa-plus"></i>&nbsp;Add User</a>
					</div>
				<div class="col-sm-10">

					<div class="pull-right">
						<a href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;
echo $_smarty_tpl->tpl_vars['_module']->value;?>
/index/admin" class="btn btn-warning btn-xs btn-w-m">Adminisrators</a>
						<a href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;
echo $_smarty_tpl->tpl_vars['_module']->value;?>
/index/registered" class="btn btn-primary btn-xs btn-w-m">Registered Users</a>
					</div>
				</div>
				</div>

			<div class="hr-line-dashed"></div>
				<?php if ($_smarty_tpl->tpl_vars['data']->value) {?>
			   <table class="table table-hover dtable" >
					<thead>
					<tr>
						<th>Username</th>
						<th>Email Address</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>User Group</th>
						<th>Actions</th>
					</tr>
					</thead>
					<tbody>
					<?php
$_from = $_smarty_tpl->tpl_vars['data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
					<tr>
						<td><a href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;
echo $_smarty_tpl->tpl_vars['_module']->value;?>
/view/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_user'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['username'];?>
</a></td>
						<td><?php echo $_smarty_tpl->tpl_vars['item']->value['email'];?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['item']->value['first_name'];?>
</td>
						<td><?php echo $_smarty_tpl->tpl_vars['item']->value['last_name'];?>
</td>
						<td><?php if ($_smarty_tpl->tpl_vars['item']->value['id_user_group'] == 1) {?>Admin<?php } else { ?>User<?php }?></td>
						<td>
							
							<a href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;
echo $_smarty_tpl->tpl_vars['_module']->value;?>
/view/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_user'];?>
" class="btn btn-warning btn-xs" title="View"><i class="fa fa-search"></i></a>
							<a href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;
echo $_smarty_tpl->tpl_vars['_module']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['item']->value['id_user'];?>
" class="btn btn-primary btn-xs" title="Edit"><i class="fa fa-pencil"></i></a>
							<a href="javascript:;" class="btn btn-danger btn-xs delete" title="Delete" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_user'];?>
" data-label="<?php echo $_smarty_tpl->tpl_vars['item']->value['username'];?>
"><i class="fa fa-trash"></i></a>

							<?php if ($_smarty_tpl->tpl_vars['item']->value['status'] == 5) {?>
							<a href="javascript:;" class="btn btn-info btn-xs resend" title="Resend Confirmation Email" data-email="<?php echo $_smarty_tpl->tpl_vars['item']->value['email'];?>
" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_user'];?>
" data-label="<?php echo $_smarty_tpl->tpl_vars['item']->value['username'];?>
"><i class="fa fa-mail-forward"></i></a>							
							<?php } else { ?>
							<?php }?>
						</td>
					</tr>					
					<?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
					</tbody>
				</table>
				<?php } else { ?>
				<p>Nothing to show. <a href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;
echo $_smarty_tpl->tpl_vars['_module']->value;?>
/add" title="Add User">Add User</a></p>
				<?php }?>
				
			</div>

		</div>	
	
	</div>
</div><?php }
}
?>