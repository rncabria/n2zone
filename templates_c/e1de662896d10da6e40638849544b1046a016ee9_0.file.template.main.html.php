<?php /* Smarty version 3.1.27, created on 2015-10-24 01:20:27
         compiled from "/Applications/mamp/apache2/htdocs/n2zone/templates/template.main.html" */ ?>
<?php
/*%%SmartyHeaderCode:1449537002562b3f4bc9ab97_87200608%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e1de662896d10da6e40638849544b1046a016ee9' => 
    array (
      0 => '/Applications/mamp/apache2/htdocs/n2zone/templates/template.main.html',
      1 => 1445674821,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1449537002562b3f4bc9ab97_87200608',
  'variables' => 
  array (
    '_base' => 0,
    '_sitename' => 0,
    '_module' => 0,
    '_theme' => 0,
    '_method' => 0,
    '_GLOBALS' => 0,
    '_template' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_562b3f4bdf07f9_72289008',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_562b3f4bdf07f9_72289008')) {
function content_562b3f4bdf07f9_72289008 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1449537002562b3f4bc9ab97_87200608';
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;?>
" />
    <title><?php echo $_smarty_tpl->tpl_vars['_sitename']->value;?>
 | <?php echo ucfirst($_smarty_tpl->tpl_vars['_module']->value);?>
</title>

    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <!-- <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/animate.css" rel="stylesheet"> -->
	<link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/chosen/chosen.css" rel="stylesheet">
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/iCheck/custom.css" rel="stylesheet">
	
	<?php if ($_smarty_tpl->tpl_vars['_module']->value == 'calendar' || $_smarty_tpl->tpl_vars['_module']->value == 'events') {?>
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>	
	<?php }?>
    <!-- Data Tables -->
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/dataTables/dataTables.tableTools.min.css" rel="stylesheet">	
	
	<link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    
    <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'maps' || $_smarty_tpl->tpl_vars['_module']->value == 'files' || ($_smarty_tpl->tpl_vars['_module']->value == 'dashboard' && $_smarty_tpl->tpl_vars['_method']->value == 'index') || ($_smarty_tpl->tpl_vars['_module']->value == 'projects' && $_smarty_tpl->tpl_vars['_method']->value == 'view')) {?>
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/dropzone/dropzone.css" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/plugins/gmap3/gmap3-menu.css" />
	<?php }?>

	
	<link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/summernote/summernote.css" rel="stylesheet">
	<link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
	
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/style.css" rel="stylesheet">
	<link href="templates/custom.css" rel="stylesheet">
	<link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['logo'];?>
" />
	<?php echo '<script'; ?>
 type="text/javascript">
	var base = '<?php echo $_smarty_tpl->tpl_vars['_base']->value;?>
';
	var module = '<?php echo $_smarty_tpl->tpl_vars['_module']->value;?>
';
	var module_view = base + module + '/view/';
	var process = base + module + '/process/';
	<?php echo '</script'; ?>
>	
	

</head>

<body class="pace-done">
    <div id="wrapper">
		<?php echo $_smarty_tpl->getSubTemplate ("include.sidebar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        <div id="page-wrapper" class="gray-bg">
			<?php echo $_smarty_tpl->getSubTemplate ("include.navbar.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

			<?php echo $_smarty_tpl->getSubTemplate ("include.header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


			<div class="wrapper wrapper-content animated fadeInDown">
				<?php if ($_smarty_tpl->tpl_vars['_template']->value) {
echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['_template']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
}?>
			</div>
			<?php echo $_smarty_tpl->getSubTemplate ("include.footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

        </div>
    </div>

    <!-- Mainly scripts -->
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/jquery-2.1.1.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/jquery-ui/jquery-ui.min.js"><?php echo '</script'; ?>
>
<?php if ($_smarty_tpl->tpl_vars['_module']->value == 'calendar' || $_smarty_tpl->tpl_vars['_module']->value == 'events') {?>
<!-- jQuery UI custom -->
<?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/jquery-ui.custom.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/fullcalendar/moment.min.js"><?php echo '</script'; ?>
>	
<?php }?>
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/metisMenu/jquery.metisMenu.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/slimscroll/jquery.slimscroll.min.js"><?php echo '</script'; ?>
>
	

    <!-- Custom and plugin javascript -->
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/inspinia.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/pace/pace.min.js"><?php echo '</script'; ?>
>

	<?php if ($_smarty_tpl->tpl_vars['_module']->value == 'notes' || $_smarty_tpl->tpl_vars['_module']->value == 'discussions' || $_smarty_tpl->tpl_vars['_module']->value == 'forum') {?>
    <!-- SUMMERNOTE -->
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/summernote/summernote.min.js"><?php echo '</script'; ?>
>	
	<?php }?>	
    <!-- GRITTER -->
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/gritter/jquery.gritter.min.js"><?php echo '</script'; ?>
>
	
	<?php if ($_smarty_tpl->tpl_vars['_module']->value == 'files' || $_smarty_tpl->tpl_vars['_module']->value == 'users' || $_smarty_tpl->tpl_vars['_module']->value == 'references') {?>
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/jquery.imgupload2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/jquery.say-cheese.js"><?php echo '</script'; ?>
>
	<?php }?>

<!-- iCheck -->
<?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/iCheck/icheck.min.js"><?php echo '</script'; ?>
>

<?php if ($_smarty_tpl->tpl_vars['_module']->value == 'calendar' || $_smarty_tpl->tpl_vars['_module']->value == 'events') {?>
<!-- Full Calendar -->

<?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/fullcalendar/fullcalendar.min.js"><?php echo '</script'; ?>
>	
<?php }?>
	
    <!-- Data Tables -->
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/dataTables/jquery.dataTables.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/dataTables/dataTables.bootstrap.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/dataTables/dataTables.responsive.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/dataTables/dataTables.tableTools.min.js"><?php echo '</script'; ?>
>	
	

    <!-- Chosen -->
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/chosen/chosen.jquery.js"><?php echo '</script'; ?>
>

   <!-- Input Mask
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/jasny/jasny-bootstrap.min.js"><?php echo '</script'; ?>
>-->

   <!-- Data picker -->
   <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/datapicker/bootstrap-datepicker.js"><?php echo '</script'; ?>
>	
<!-- DROPZONE -->
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/dropzone/dropzone.js"><?php echo '</script'; ?>
>	
	
	    <!-- Jquery Validate -->
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/validate/jquery.validate.min.js"><?php echo '</script'; ?>
>	
	
<!--
You need to include this script on any page that has a Google Map.
When using Google Maps on your own site you MUST signup for your own API key at:
https://developers.google.com/maps/documentation/javascript/tutorial#api_key
After your sign up replace the key in the URL below..
-->
<?php if ($_smarty_tpl->tpl_vars['_module']->value == 'maps' || ($_smarty_tpl->tpl_vars['_module']->value == 'dashboard' && $_smarty_tpl->tpl_vars['_method']->value == 'index') || ($_smarty_tpl->tpl_vars['_module']->value == 'projects' && $_smarty_tpl->tpl_vars['_method']->value == 'view')) {?>
	<?php echo '<script'; ?>
 type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/plugins/gmap3/gmap3.min.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/plugins/gmap3/gmap3-menu.js"><?php echo '</script'; ?>
>
<?php }?>
	<?php echo '<script'; ?>
 src="templates/globals.js" ><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="templates/js/<?php echo $_smarty_tpl->tpl_vars['_module']->value;?>
.js"><?php echo '</script'; ?>
>

    <!-- Toastr -->
    <?php echo '<script'; ?>
 src="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/js/plugins/toastr/toastr.min.js"><?php echo '</script'; ?>
>

	
</body>
</html>
<?php }
}
?>