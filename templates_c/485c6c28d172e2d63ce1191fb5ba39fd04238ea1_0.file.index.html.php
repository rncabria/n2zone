<?php /* Smarty version 3.1.27, created on 2015-10-13 01:53:13
         compiled from "/Applications/mamp/apache2/htdocs/n2zone/templates/modules/aje/index.html" */ ?>
<?php
/*%%SmartyHeaderCode:424376865561cc679843647_85158848%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '485c6c28d172e2d63ce1191fb5ba39fd04238ea1' => 
    array (
      0 => '/Applications/mamp/apache2/htdocs/n2zone/templates/modules/aje/index.html',
      1 => 1444726241,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '424376865561cc679843647_85158848',
  'variables' => 
  array (
    '_GLOBALS' => 0,
    'data' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561cc67987e285_07704107',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561cc67987e285_07704107')) {
function content_561cc67987e285_07704107 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '424376865561cc679843647_85158848';
?>
<div class="ibox float-e-margins">
    <div class="ibox-content p-md">
        <h2>
        	<span class="text-navy"><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['_GLOBALS']->value['module'], 'UTF-8');?>
</span>
			<button type="button" class="btn btn-success btn-w-m add pull-right" data-toggle="modal" data-target="#add-modal"><i class="fa fa-plus"></i>&nbsp;Add</button>
        </h2>


	<hr />
	<?php if ($_smarty_tpl->tpl_vars['data']->value) {?>
   <table class="table table-hover dtable" >
		<thead>
		<tr>
			<th>Name</th>
			<th>Description</th>
			<th>TIN</th>
			<th width="60px;">Actions</th>
		</tr>
		</thead>
		<tbody>
		<?php
$_from = $_smarty_tpl->tpl_vars['data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
		<tr>
			<td><a href="javascript:;"><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</a></td>
			<td><?php echo $_smarty_tpl->tpl_vars['item']->value['description'];?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['item']->value['tins'];?>
</td>
			<td>	
				<a href="javascript:;" class="btn btn-xs btn-primary edit" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_forum_category'];?>
"><i class="fa fa-pencil"></i></a>
				<a href="javascript:;" class="btn btn-danger btn-xs delete" title="Delete" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_forum_category'];?>
" data-label="<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
" data-action="delete-category"><i class="fa fa-trash"></i></a>
			</td>
		</tr>					
		<?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
		</tbody>
	</table>
	<?php } else { ?>
	<p>Nothing to show.
	<?php }?>

    </div>
</div>


<div class="modal inmodal" id="add-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-bookmark-o modal-icon"></i>
				<h4 class="modal-title">Add Category</h4>
				
			</div>
			<div class="modal-body">
				<form role="form" id="form-category">	
					<div class="form-group">
						<input type="hidden" name="data[id_forum_category]" value="0" id="m-id" />
						<input type="text" name="data[name]" placeholder="Category" class="form-control" required id="m-name" />
					</div>
					<!-- <div class="form-group">
						<input type="text" name="data[code]" placeholder="Code" class="form-control" required id="m-code" maxlength="10" />
					</div> -->
					<div class="form-group">
						<textarea class="form-control" name="data[description]" rows="5" placeholder="Description" id="m-description"></textarea>
					</div>
				</form>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="category-btn" data-action="category">Save changes</button>
			</div>
		</div>
	</div>
</div><?php }
}
?>