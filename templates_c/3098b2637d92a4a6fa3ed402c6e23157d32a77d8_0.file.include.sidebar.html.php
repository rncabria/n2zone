<?php /* Smarty version 3.1.27, created on 2015-10-13 02:10:02
         compiled from "/Applications/mamp/apache2/htdocs/n2zone/templates/include.sidebar.html" */ ?>
<?php
/*%%SmartyHeaderCode:1641263844561cca6af3b555_25150351%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3098b2637d92a4a6fa3ed402c6e23157d32a77d8' => 
    array (
      0 => '/Applications/mamp/apache2/htdocs/n2zone/templates/include.sidebar.html',
      1 => 1444727356,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1641263844561cca6af3b555_25150351',
  'variables' => 
  array (
    '_GLOBALS' => 0,
    '_module' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561cca6b096f33_12291178',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561cca6b096f33_12291178')) {
function content_561cca6b096f33_12291178 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1641263844561cca6af3b555_25150351';
?>
<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element"> 
					<span><img alt="image" class="img-circle" src="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['logo'];?>
" width="48px" height="48px" /></span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
						 <span class="text-muted text-xs block m-t-xs">
						 	<strong class="font-bold"><?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['user']['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['user']['last_name'];?>
</strong> 
						 	<b class="caret"></b>
						 </span>
					</a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
account">My Account</a></li>
						<li class="divider"></li>
						<li><a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
dashboard/logout">Logout</a></li>
                    </ul>
                </div>				
				<div class="logo-element">
					<img alt="image" class="img-circle" src="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['logo'];?>
" width="48px" height="48px" />
				</div>
			</li>
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'dashboard') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
" title="Dashboard"><i class="fa fa-home"></i> <span class="nav-label">Dashboard</span></a>
			</li>
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'sales') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
sales" title="Sales"><i class="fa fa-barcode"></i> <span class="nav-label">Sales</span></a>
			</li>
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'purchases') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
purchases" title="Purchases"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Purchases</span></a>
			</li>
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'disbursements') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
disbursements" title="Disbursements"><i class="fa fa-sitemap"></i> <span class="nav-label">Disbursements</span></a>
			</li>
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'aje') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
aje" title="AJE"><i class="fa fa-tasks"></i> <span class="nav-label">AJE</span></a>
			</li>
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'ledger') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
ledger" title="Ledger"><i class="fa fa-align-justify"></i> <span class="nav-label">Ledger</span></a>
			</li>			
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'report') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
report" title="Report"><i class="fa fa-area-chart"></i> <span class="nav-label">Report</span></a>
			</li>
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'client') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
clients" title="Clients"><i class="fa fa-users"></i> <span class="nav-label">Clients</span></a>
			</li>
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'users') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
users" title="Users"><i class="fa fa-user"></i> <span class="nav-label">Users</span></a>
			</li>
			<li <?php if ($_smarty_tpl->tpl_vars['_module']->value == 'settings') {?>class="active"<?php }?>>
				<a href="<?php echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['base'];?>
dashboard/settings" title="Settings"><i class="fa fa-cogs"></i> <span class="nav-label">Settings</span></a>
			</li>			
		</ul>
	</div>
</nav><?php }
}
?>