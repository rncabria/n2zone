<?php /* Smarty version 3.1.27, created on 2015-10-11 22:08:27
         compiled from "/Applications/mamp/apache2/htdocs/n2zone/templates/modules/client/ae.html" */ ?>
<?php
/*%%SmartyHeaderCode:507611629561b404b6106f8_47785340%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a0400908f866a5fad44b4c213a9dda0e9d6d841b' => 
    array (
      0 => '/Applications/mamp/apache2/htdocs/n2zone/templates/modules/client/ae.html',
      1 => 1440346980,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '507611629561b404b6106f8_47785340',
  'variables' => 
  array (
    '_method' => 0,
    'data' => 0,
    '_theme' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561b404b710659_25151736',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561b404b710659_25151736')) {
function content_561b404b710659_25151736 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '507611629561b404b6106f8_47785340';
?>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title ibox-<?php echo $_smarty_tpl->tpl_vars['_method']->value;?>
"><h5><?php echo ucfirst($_smarty_tpl->tpl_vars['_method']->value);?>
 User</h5></div>
			<div class="ibox-content">
				<form method="post" class="form-horizontal" enctype="multipart/form-data">
					<div class="form-group">
						<label class="col-sm-2 control-label">Email Address</label>
						<div class="col-sm-10"><input type="text" name="data[email]" class="form-control" placeholder="Email" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['email'];?>
"></div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">User Group</label>
						<div class="col-sm-10">
							<select name="data[id_user_group]" class="form-control" required>	
								<option value=""></option>
								<option value="1" <?php if ($_smarty_tpl->tpl_vars['data']->value['id_user_group'] == 1) {?>selected="selected"<?php }?>>Administrator</option>
								<option value="2" <?php if ($_smarty_tpl->tpl_vars['data']->value['id_user_group'] == 2) {?>selected="selected"<?php }?>>Registered User</option>								
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Status</label>
						<div class="col-sm-10">
							<select name="data[status]" class="form-control" required>	
								<option value=""></option>
								<option value="1" <?php if ($_smarty_tpl->tpl_vars['data']->value['status'] == 1) {?>selected="selected"<?php }?>>Active</option>
								<option value="3" <?php if ($_smarty_tpl->tpl_vars['data']->value['status'] == 3) {?>selected="selected"<?php }?>>Inactive</option>								
							</select>
						</div>
					</div>						
					<div class="form-group">
						<label class="col-sm-2 control-label">Username</label>
						<div class="col-sm-10"><input type="text" name="data[username]" class="form-control" placeholder="Username" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['username'];?>
" maxlength="30" ></div>
					</div>
					<?php if ($_smarty_tpl->tpl_vars['_method']->value == 'add') {?>
					<div class="form-group">
						<label class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10"><input type="password" name="data[password]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['password'];?>
"></div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Confirm Password</label>
						<div class="col-sm-10"><input type="password" name="passconf" class="form-control"></div>
					</div>
					<?php } else { ?>
					<div class="form-group">
						<label class="col-sm-2 control-label">&nbsp;</label>
						<input type="hidden" name="data[id_user]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id_user'];?>
" />
						<div class="col-sm-10"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#change-password-modal">Change Password</button></div>
					</div>
					
					
					<?php }?>

					<div class="hr-line-dashed"></div>

					<div class="form-group">
						<label class="col-sm-2 control-label">First Name</label>
						<div class="col-sm-10"><input type="text" name="data[first_name]" class="form-control" placeholder="First Name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['first_name'];?>
"></div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Middle Name</label>
						<div class="col-sm-10"><input type="text" name="data[middle_name]" class="form-control" placeholder="Middle Name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['middle_name'];?>
" maxlength="50" /></div>
					</div>					
					<div class="form-group">
						<label class="col-sm-2 control-label">Last Name</label>
						<div class="col-sm-10"><input type="text" name="data[last_name]" class="form-control" placeholder="Last Name" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['last_name'];?>
" maxlength="50" /></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">Affiliation</label>
						<div class="col-sm-10"><input type="text" name="data[affiliation]" class="form-control" placeholder="Affiliation" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['affiliation'];?>
"></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">Expertise</label>
						<div class="col-sm-10"><input type="text" name="data[expertise]" class="form-control" placeholder="Expertise" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['expertise'];?>
"></div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Contact Number</label>
						<div class="col-sm-10"><input type="text" name="data[contact_number]" class="form-control" placeholder="Contact Number" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['contact_number'];?>
" maxlength="30" /></div>
					</div>	
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['_theme']->value)."/include.form.footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="change-password-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-unlock-alt modal-icon"></i>
				<h4 class="modal-title">Change Password</h4>
			</div>
			<div class="modal-body">
				<form role="form" id="change_pass_form">	
					<div class="form-group">
						<label>Current Password *</label>
						<input id="password" name="old_pwd" type="password" class="form-control required" maxlength="50">
						<input type="hidden" name="id_user" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id_user'];?>
" class="form-control required" maxlength="50">
					</div>
					<div class="form-group">
						<label>New Password *</label>
						<input name="new_pwd" type="password" class="form-control required" maxlength="50">
					</div>					
					<div class="form-group">
						<label>Confirm New Password *</label>
						<input id="confirm" name="passconf" type="password" class="form-control required" maxlength="50">
					</div>
				</form>
			</div>
			<div class="modal-footer" id="change_pass_footer">
				<div class="sk-spinner sk-spinner-three-bounce" style="display:none;">
					<div class="sk-bounce1"></div>
					<div class="sk-bounce2"></div>
					<div class="sk-bounce3"></div>
				</div>
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary change-pwd-button">Save changes</button>
			</div>
		</div>
	</div>
</div><?php }
}
?>