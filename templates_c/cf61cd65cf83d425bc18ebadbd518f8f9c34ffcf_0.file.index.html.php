<?php /* Smarty version 3.1.27, created on 2015-10-24 09:14:38
         compiled from "/Applications/mamp/apache2/htdocs/n2zone/templates/modules/sales/index.html" */ ?>
<?php
/*%%SmartyHeaderCode:964171250562bae6e394844_25856166%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cf61cd65cf83d425bc18ebadbd518f8f9c34ffcf' => 
    array (
      0 => '/Applications/mamp/apache2/htdocs/n2zone/templates/modules/sales/index.html',
      1 => 1445703192,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '964171250562bae6e394844_25856166',
  'variables' => 
  array (
    '_GLOBALS' => 0,
    'i' => 0,
    'filter' => 0,
    'data' => 0,
    'item' => 0,
    'total' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_562bae6e47a2c7_01721691',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_562bae6e47a2c7_01721691')) {
function content_562bae6e47a2c7_01721691 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/Applications/mamp/apache2/htdocs/n2zone/application/third_party/Smarty/plugins/modifier.date_format.php';

$_smarty_tpl->properties['nocache_hash'] = '964171250562bae6e394844_25856166';
?>
<div class="ibox">
    <div class="ibox-content p-md">
        <h2>
        	<span class="text-navy"><?php echo mb_strtoupper($_smarty_tpl->tpl_vars['_GLOBALS']->value['module'], 'UTF-8');?>
</span>
        </h2>

	<form class="form-horizontal" role="form" action="" method="post">
		<select name="filter[month]" placeholder="Month">
			<option value=""> - MONTH - </option>
			<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 12+1 - (1) : 1-(12)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['filter']->value['month'] == $_smarty_tpl->tpl_vars['i']->value) {?>selected="selected"<?php }?>><?php echo smarty_modifier_date_format("1970/".((string)$_smarty_tpl->tpl_vars['i']->value)."/01","%B");?>
</option>
			<?php }} ?>
		</select>
		<select name="filter[year]" >
			<option value=""> - YEAR - </option>
			<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? smarty_modifier_date_format(time(),"%Y")+1+1 - (2015) : 2015-(smarty_modifier_date_format(time(),"%Y")+1)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 2015, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
			<option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['filter']->value['year'] == $_smarty_tpl->tpl_vars['i']->value) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
			<?php }} ?>
		</select>
		<input type="submit" class="btn btn-success btn-xs" value="GO" />

			<button type="button" class="btn btn-success btn-w-m add pull-right" data-toggle="modal" data-target="#add-modal"><i class="fa fa-plus"></i>&nbsp;Add</button>		
	</form>        
	<hr />

<?php if ($_smarty_tpl->tpl_vars['data']->value) {?>
   <table class="table table-hover dtable" >
		<thead>
		<tr>
			<th>Date</th>
			<th>Particulars</th>
			<th>TIN</th>
			<th>Invoice #</th>
			<th>Services</th>
			<th>Output %</th>
			<th>Cash</th>
			<th width="60px;">Actions</th>
		</tr>
		</thead>
		<tbody>
		<?php
$_from = $_smarty_tpl->tpl_vars['data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
		<tr>
			<td><?php echo $_smarty_tpl->tpl_vars['item']->value['date'];?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['item']->value['particulars'];?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['item']->value['tin'];?>
</td>
			<td><?php echo $_smarty_tpl->tpl_vars['item']->value['invoice_no'];?>
</td>
			<td><?php echo number_format($_smarty_tpl->tpl_vars['item']->value['services_cr'],"2",".",",");?>
</td>
			<td><?php echo number_format($_smarty_tpl->tpl_vars['item']->value['output_percent_cr'],"2",".",",");?>
</td>
			<td><?php echo number_format($_smarty_tpl->tpl_vars['item']->value['cash_dr'],"2",".",",");?>
</td>
			<td>	
				<a href="javascript:;" class="btn btn-xs btn-primary edit" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_acct_sales'];?>
"><i class="fa fa-pencil"></i></a>
				<a href="javascript:;" class="btn btn-danger btn-xs delete" title="Delete" data-id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id_acct_sales'];?>
" data-label="<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
" data-action="delete-category"><i class="fa fa-trash"></i></a>
			</td>
		</tr>					
		<?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
		</tbody>
		<tfoot>
		<tr>
			<th>TOTAL</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th><?php echo number_format($_smarty_tpl->tpl_vars['total']->value['services_cr'],"2",".",",");?>
</th>
			<th><?php echo number_format($_smarty_tpl->tpl_vars['total']->value['output_percent_cr'],"2",".",",");?>
</th>
			<th><?php echo number_format($_smarty_tpl->tpl_vars['total']->value['cash_dr'],"2",".",",");?>
</th>
			<th width="60px;">&nbsp;</th>
		</tr>			
		</tfoot>
	</table> 
	
	<?php } else { ?>
	<p>Nothing to show.</p>
	<?php }?>

    </div>
</div>


<div class="modal inmodal" id="add-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<!-- <i class="fa fa-bookmark-o modal-icon"></i> -->
				<h4 class="modal-title">ADD <?php echo mb_strtoupper($_smarty_tpl->tpl_vars['_GLOBALS']->value['module'], 'UTF-8');?>
</h4>
				
			</div>
			<div class="modal-body">
				<form role="form">	
					<div class="form-group">
						<input type="text" name="data[date]" placeholder="Date (yyyy-mm-dd)" class="form-control datepicker" required  />
					</div>
					<div class="form-group">
						<input type="text" name="data[particulars]" placeholder="Particulars" class="form-control" required  />
					</div>
					<div class="form-group">
						<input type="text" name="data[tin]" placeholder="TIN (xxx-xxx-xxx-xxx)" class="form-control" required  />
					</div>
					<div class="form-group">
						<input type="text" name="data[invoice_no]" placeholder="Invoice #" class="form-control" required  />
					</div>
					<div class="form-group">
						<input type="text" name="data[cash_dr]" placeholder="Cash Dr. (0.00)" class="form-control" required  id="cash_dr" />
					</div>
					<div class="form-group">
						<input type="text" name="data[output_percent_cr]" placeholder="Output % Cr. (0.00)" class="form-control" required id="output_cr" readonly />
					</div>					
					<div class="form-group">
						<input type="text" name="data[services_cr]" placeholder="Services Cr. (0.00)" class="form-control" required id="services_cr" readonly />
					</div>


				</form>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary save">Save changes</button>
			</div>
		</div>
	</div>
</div><?php }
}
?>