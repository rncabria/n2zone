<?php /* Smarty version 3.1.27, created on 2015-10-10 00:03:43
         compiled from "/Applications/mamp/apache2/htdocs/n2zone/templates/template.login.html" */ ?>
<?php
/*%%SmartyHeaderCode:3981393555618b84f8ca7c0_22253671%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a030088d306a4c6f690a7c8dde336ea845451779' => 
    array (
      0 => '/Applications/mamp/apache2/htdocs/n2zone/templates/template.login.html',
      1 => 1444460565,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3981393555618b84f8ca7c0_22253671',
  'variables' => 
  array (
    '_base' => 0,
    '_sitename' => 0,
    '_theme' => 0,
    'error' => 0,
    '_GLOBALS' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5618b84f917057_57925135',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5618b84f917057_57925135')) {
function content_5618b84f917057_57925135 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '3981393555618b84f8ca7c0_22253671';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;?>
" />
    <title><?php echo $_smarty_tpl->tpl_vars['_sitename']->value;?>
 | Login</title>

    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/animate.css" rel="stylesheet">
    <link href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/css/style.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/<?php echo $_smarty_tpl->tpl_vars['_theme']->value;?>
/logo.jpg" />
</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
                <h2 class="font-bold">Welcome to <?php echo $_smarty_tpl->tpl_vars['_sitename']->value;?>
</h2>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque eget nulla neque. Nulla maximus tincidunt ipsum, ut suscipit tellus ornare convallis. Proin vestibulum eget orci nec ultricies. Vivamus sit amet tempus risus, id vehicula lacus. Proin in ultricies nisl. 
                </p>

                <p>
                    Mauris euismod sit amet nulla vel interdum. Etiam eget elit malesuada, tincidunt lorem eu, posuere massa. Donec sed arcu sed augue auctor sodales. Nunc dapibus vitae ex quis lacinia.
                </p>

                <p>
                    <small>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</small>
                </p>

            </div>
            <div class="col-md-6">
                <div class="ibox-content">
					<?php if ($_smarty_tpl->tpl_vars['error']->value) {?><p style="color:red"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }?>
                    <form class="m-t" role="form" action="" method="post">
                        <div class="form-group">
                            <input name="data[username]" type="text" class="form-control" placeholder="Username" required="">
                        </div>
                        <div class="form-group">
                            <input name="data[password]" type="password" class="form-control" placeholder="Password" required="">
                        </div>
                        <button type="submit" id="login" class="btn btn-primary block full-width m-b">Login</button>

                        <a href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;
echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['module'];?>
/forgot_password">
                            <small>Forgot password?</small>
                        </a>

                        <p class="text-muted text-center">
                            <small>Do not have an account?</small>
                        </p>
                        <a class="btn btn-sm btn-white btn-block" href="<?php echo $_smarty_tpl->tpl_vars['_base']->value;
echo $_smarty_tpl->tpl_vars['_GLOBALS']->value['module'];?>
/register">Create an account</a>
                    </form>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                <strong>Copyright</strong> <?php echo $_smarty_tpl->tpl_vars['_sitename']->value;?>

            </div>
            <div class="col-md-6 text-right">
               <small>&copy; 2015</small>
            </div>
        </div>
    </div>
</body>

</html>
<?php }
}
?>