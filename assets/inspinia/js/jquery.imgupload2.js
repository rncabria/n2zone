/**
 *	Author: Darril Louie Ramos
 *	Description: Transforms input file tags into preview-upload images.
 *	
 *	Required input: <input type="file" data-img-src="image_url_here" /> enclosed in DIV "display:block;" with desired width
 *	Sample Usage: $(selector).imgupload2();
 *
 */

;(function($) {
	
	var methods = {
		init : function (options)
			{
				
				var file_img_input = this;
				var instance_index = 0;
				var attr_id = "imgupload";
				var id = "";
				var image_holder = this.parent();
				var img_src = (this.data('img-src')) ? this.data('img-src') : '';
				var img_display_style = '';
				var image_holder = this.parent();
				
				if(this.attr("type") !== "file") return false;
				
				this.hide();
				
				for(var count = 1; $(attr_id).length > 0; count++){
					attr_id += "_"+count;
				}
				
				id = attr_id;
				
				if(img_src == ''){img_display_style = 'display:none;'}
				
				var defaults = {
					select_button_html : '<a class="btn btn-small btn-success" id="'+id+'-upload-btn" href="javascript:;" >Select Image</a>',
					remove_button_html : '<a class="btn btn-small btn-danger" id="'+id+'-remove-btn" href="javascript:;" title="Remove" style="'+img_display_style+';margin-left:3px;font-weight: bold;" >&times;</a>',
					container_html : "",
					container_width : "",
					container_height : ""
				};
				
				var opts = $.extend({}, defaults, options);
				
				image_holder.append(
					'<div style="display:inline-block;text-align:left;" class="image-holder">'+
					opts.select_button_html+
					opts.remove_button_html+
					'<div id="'+id+'-div-rel-container" style="width:100%;position:relative;">'+
					'<div style="height:100%;width:100%;"><img class="img" src="'+img_src+'" style="max-width:'+image_holder.css('width')+';'+img_display_style+'" /></div>'+
					'</div>'+
					'</div>'
				);
				
				this.attr("accept", "image/*");
				
				$('#'+id+'-upload-btn').click(function() {
					file_img_input.click();
				});
				
				this.change(function(){
					
					if($('#'+id+'-img-input').val()!=''){
						prev_img = image_holder.find('.img');
						
						if(img_src == ''){
							image_holder.find('.img').css('padding', '0px');
							image_holder.find('div').unbind();
						}
						
						if (this.files && this.files[0]) {
							var reader = new FileReader();
							
							if(!this.value.match(/\.(jpg|jpeg|png|gif|bmp)$/i)){
								alert('File type not allowed. Upload image files only.');
							}else{
								reader.onload = function (e) {
									image_holder.find('.img').attr('src', e.target.result);
									
									image_holder.find('.img').unbind();
									image_holder.find('.img').mouseover(function() {
									});
									image_holder.find('.img').mouseout(function() {
									});
									
									image_holder.find('.img').show();
								}
								reader.readAsDataURL(this.files[0]);
							}
						}
						
						$('#'+id+'-remove-btn').show();
					}
				});
				
				$('#'+id+'-remove-btn').unbind();
				$('#'+id+'-remove-btn').click(function(){
					$(this).hide();
					image_holder.find('.img').hide();
					file_img_input.val('');
				});
				
				return false;
			},
		refresh : function ()
			{
				var id = this.attr('id');
				var image_holder = this.parent();
				var file_path = this.data('file-path');
			
				if($('#'+id).val() != ''){
					image_holder.find('.img').attr('src', file_path+''+$('#'+id).val());
					$('#'+id+'-remove-btn').show();
					image_holder.find('.img').parent().css('width', '100%');
					image_holder.find('.img').parent().show();
					image_holder.find('.img').show();
				}else{
					image_holder.find('.img').attr('src', '');
					$('#'+id+'-remove-btn').hide();
					image_holder.find('.img').parent().css('width', '0%');
					image_holder.find('.img').hide();
				}
			},
		clear : function ()
			{
				var id = this.attr('id');
				var image_holder = this.parent();
				
				this.val('');
				image_holder.find('.img').attr('src', '');
				$('#'+id+'-remove-btn').hide();
				image_holder.find('.img').hide();
			},
		reset : function ()
			{
				var id = this.attr('id');
				var image_holder = this.parent();
				var img_src = this.data('img-src');
			
				this.val('');
				image_holder.find('.img').attr('src', img_src);
				$('#'+id+'-remove-btn').hide();
			}
	};
	
	$.fn.imgupload2 = function(methodOrOptions)
	{
		if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
			return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.imgupload' );
        }    
	}
	
})(jQuery);