<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ejae_client extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ejae_client_model', 'md');
	}

	function index() {
		$data = $this->md->get();
		$this->smarty->assign('data', $data);

		// pinapasa mo kay index function ung data from model
	}	

	function process($action) {
		switch($action) {
			case '' : break;		
			default : break;
		}
		exit(0);
	}
}
