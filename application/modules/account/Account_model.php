<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends CI_Model {
    function __construct() {
       parent::__construct();
	   
	}
	
	function login($data) {
		$this->db->where('username', $data['username']);
		$this->db->where('password', sha1($data['password']));
		$query = $this->db->get('user_acct');
		return $query->num_rows() ? $query->row_array() : false;
	}
	
	function getCookies($token) {
		$this->db->where('token', $token);
		$query = $this->db->get('cookies');
		return $query->num_rows() ? $query->row_array() : false;
	}
	
	function setCookies($ip, $data) {
		$data = json_encode($data);
		$token = sha1($ip . $data);
		$date_expire = time()+60*60*24*30;//date('Y-m-d', strtotime("+30 days"));
		
		$values = array(
			'token' => $token
			,'ip' => $ip
			,'data' => $data
			,'date_expire' => $date_expire
		);
		
		if($this->dbtm->getter('cookies', array('token' => $token), false, false, true)) {
			$this->db->where('token', $token);
			$this->db->update('cookies', $values);
		} else $this->db->insert('cookies', $values);
			
		setcookie(COOKIE, $token, $date_expire);		
	}

	function forgotPassword($email) {		
		$this->db->where('email', $email);
		$this->db->where('status >',0);
		$query = $this->db->get('user_acct');
		
		if($query->num_rows()) {
			$data = $query->row_array();
			if($data['status'] == 5) return 5; //NEED TO CONFIRM EMAIL ADDRESS FIRST;
			
			$data = array(
				'email' => $data['email'],
				'token' => base64_encode( date('Y/m/d h:i:sa').$data['email']),
				'expire' => date('Y/m/d h:i:sa',strtotime(date('Y/m/d h:i:sa') . ' +1 day')),
				'id_user_account' => $data['id_user_account'],
				'ip' => $_SERVER['REMOTE_ADDR']
			);			
			$this->db->insert('forgot_password', $data);
			return $this->db->insert_id() ? 1 : 2;
		} else {
			return 0;//email doesnt exists;
		}
	}
	
	function setLoginActivity($data, $status, $notes = false, $username = false) {
		//status 0 - wrong username/pwd, 1 - success, 2 - inactive
		$values = array(
			'ip' => $_SERVER['REMOTE_ADDR'],
			'browser' => $_SERVER['HTTP_USER_AGENT'],
			'url' => $_SERVER['REQUEST_URI'],
			'data' => $data,
			'status' => $status,
			'notes' => $notes		
		);
		$this->db->insert('login_activity', $values);
	}	
	
	function register($data) {
		$this->db->trans_start();
		$data['creation_date'] = date('Y-m-d H:i:s');
		$data['id_user_group'] = 2; //REGISTERED USER
		$data['status'] = 5; //NEWLY REGISTERED NOT YET ACTIVATED
		$data['password'] = sha1($data['password']);
		$this->db->insert('user_acct', $data);
		$data['id_user_account'] = $this->db->insert_id();
		$this->db->trans_complete();
		if($this->db->trans_status() == TRUE) {			
			return $this->sendConfirmationEmail($data);
			
		} else {
			return false;
		}
	}
	
	function sendConfirmationEmail($data) {
		$this->db->where('id_user', $data['id_user_account']);
		$this->db->where('status', 5);//NEWLY REGISTERED NOT YET ACTIVATED
		$query = $this->db->get('user_acct');
		if($query->num_rows()) {
			$email['to'] = $data['email'];
			$email['subject'] = 'Verify your E-mail Address';
			$token = base64_encode(date('Y/m/d h:i:sa'));
			$verification_link = BASE . 'dashboard/verify/' . $data['id_user_account'] . '/' . $token;

			$email['body'] = 'Please click this link to verify your email address. You will be redirected to the login page.  ' . $verification_link ;
			$sent = $this->sendEmail($email);
			if($sent) {
				$conf = array(
					'id_user' => $data['id_user_account'],
					'token' => $token,
					'date_expiry' => date('Y-m-d H:i:s', strtotime("+1 day")),
				);
				$this->db->insert('verify_email', $conf);
			}
			return $sent;

			// $this->load->library('email');
			// $fromEmail = 'admin@larcph.com';
			// $fromEmailPass = 'admin1129426';
			// $smtpHost = 'mail.larcph.com';
			// $smtpPort = '25';
			
			// $config = Array(
			// 	'protocol' => "smtp",
			// 	'smtp_host' => $smtpHost,
			// 	'smtp_port' => $smtpPort,
			// 	'smtp_user' => $fromEmail,
			// 	'smtp_pass' => $fromEmailPass,
			// 	'charset'  => "utf-8",
			// 	'mailtype'  => "html",
			// 	'newline'  => "\r\n",
			// 	'crlf'  => "\r\n"
			// );
			// $this->load->library('email',$config);	
			// $this->email->from($fromEmail, SITENAME);
			// $this->email->reply_to($fromEmail, SITENAME);
			// $this->email->to($data['email']);
		
			// $this->email->subject('Verify your E-mail Address');
			// $this->email->message('Please click this link to verify your email address. You will be redirected to the login page.  ' . $verification_link );

			// if($this->email->send()) {
			// 	$conf = array(
			// 		'id_user' => $data['id_user_account'],
			// 		'token' => $token,
			// 		'date_expiry' => date('Y-m-d H:i:s', strtotime("+1 day")),
			// 	);
			// 	$this->db->insert('verify_email', $conf);
			// }
			// var_dump($this->email->print_debugger());
			// return $this->email->print_debugger();
		} else {
			return false;
		}
	}
	
	function verifyEmail($id_user, $token) {
		$this->db->where('id_user', $id_user);
		$this->db->where('token', $token);
		$query = $this->db->get('verify_email');
		if($query->num_rows()) {
			
			if(date('Y-m-d H:i:s') > $query->row()->date_expiry) return false;
			else {
				$this->db->trans_start();
				$this->db->where('id_user', $id_user);
				$this->db->where('token', $token);
				$this->db->update('verify_email', array('date_verify' => date('Y-m-d H:i:s')));
				
				$this->db->where('id_user', $id_user);
				$this->db->update('user_acct', array('status' => 6));
				$this->db->trans_complete();
				return true;			
			}
		}
		return false;
	}
	
	function checkDuplicate($data) {
		if($data['email']) $this->db->where('email', $data['email']);
		if($data['username']) $this->db->where('username', $data['username']);
		$query = $this->db->get('user_acct');
		return $query->num_rows() > 0 ? 'false' : 'true';//FORMAT for jquery validation
	}
	
	function confirmEmail($token){
		$this->db->from('forgot_password');
		$this->db->where('token',$token);
		$query = $this->db->get();
		return $query->num_rows() ? false : true;
	}
	
	function checkEmail($to) {
		$this->db->select("id_user, id_user_group, first_name, last_name, email");
		$this->db->from("user");
		$this->db->where("email", $to);
		$query = $this->db->get();
		return $query->num_rows() ? $query->row_array() : false;	
	}
	
	function checkUsername($to) {
		$this->db->select("id_user, username");
		$this->db->from("user");
		$this->db->where("username", $to);
		$query = $this->db->get();
		return $query->num_rows() ? $query->row_array() : false;	
	}

	function sendEmail($email) {
			$this->load->library('email');
			$fromEmail = 'admin@larcph.com';
			$fromEmailPass = 'admin1129426';
			$smtpHost = 'mail.larcph.com';
			$smtpPort = '25';
			
			$config = Array(
				'protocol' => "smtp",
				'smtp_host' => $smtpHost,
				'smtp_port' => $smtpPort,
				'smtp_user' => $fromEmail,
				'smtp_pass' => $fromEmailPass,
				'charset'  => "utf-8",
				'mailtype'  => "html",
				'newline'  => "\r\n",
				'crlf'  => "\r\n"
			);
			$this->load->library('email',$config);	
			$this->email->from($fromEmail, SITENAME);
			$this->email->reply_to($fromEmail, SITENAME);
			$this->email->to($email['to']);
		
			$this->email->subject($email['subject']);
			
			$this->email->message($email['body']);

			$this->email->send();
			return $this->email->print_debugger();

	}
	function sendPasswordResetEmail($data) {

		if($data) {
			$email['subject'] = 'Password Reset';
			$pwd = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 10 );
			$email['body'] = 'Hi '.$data['first_name'].'!  Your new password is '. $pwd . ', If you did not request for password reset, please contact the system administrator.';
			$email['to'] = $data['email'];
			$sent = $this->sendEmail($email);
			if($sent) {
				$this->db->where('id_user', $data['id_user_account']);
				$this->db->update('user_acct', array('password' => sha1($pwd)));				
			}
			return $sent;
			// $this->load->library('email');
			// $fromEmail = 'admin@larcph.com';
			// $fromEmailPass = 'admin1129426';
			// $smtpHost = 'mail.larcph.com';
			// $smtpPort = '25';
			
			// $config = Array(
			// 	'protocol' => "smtp",
			// 	'smtp_host' => $smtpHost,
			// 	'smtp_port' => $smtpPort,
			// 	'smtp_user' => $fromEmail,
			// 	'smtp_pass' => $fromEmailPass,
			// 	'charset'  => "utf-8",
			// 	'mailtype'  => "html",
			// 	'newline'  => "\r\n",
			// 	'crlf'  => "\r\n"
			// );
			// $this->load->library('email',$config);	
			// $this->email->from($fromEmail, SITENAME);
			// $this->email->reply_to($fromEmail, SITENAME);
			// $this->email->to($data['email']);
		
			// $this->email->subject('Password Reset');
			// $pwd = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 10 );
			// $this->email->message('Hi '.$data['first_name'].'!  Your new password is '. $pwd . ', If you did not request for password reset, please contact the system administrator.');

			// if($this->email->send()) {
			// 	$this->db->where('id_user', $data['id_user_account']);
			// 	$this->db->update('user_acct', array('password' => sha1($pwd)));
			// 	//return $this->db->affected_rows() ? 1 : false;
			// }
			// var_dump($this->email->print_debugger());
			// return $this->email->print_debugger();
		} else {
			return false;
		}
	}

}