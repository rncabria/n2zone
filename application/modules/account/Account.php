<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		$this->load->model('Account_model', 'md');
	}

	function register() {
		//Register for new account

	}

	function verify($email = false) {
		//Verify email before creating user account

	}

	function login() {
		$this->load->library('encryption');
		//Check credentials and Login user account
		$error = false;
		
		if($this->input->cookie(COOKIE)){
			

			$cookie = $this->md->getCookies($this->input->cookie(COOKIE));
			$cookie['data'] = json_decode($cookie['data'], true);

			$cookie['password'] = $this->encryption->decrypt($cookie['password']);
			$this->smarty->assign('data', $cookie);
		}
		
		if ($_POST) {
			$data = $this->input->post('data');

			$result = $this->md->login($data);
			
			if($result){
				$data['password'] = $this->encryption->encrypt($data['password']);
				if($this->input->post('remember') == 'yes') $this->md->setCookies($_SERVER['REMOTE_ADDR'], $data);					
				else setcookie(COOKIE, '', 0);
				
				if($result['status'] == 1) {
					$this->session->set_userdata('_session', $result);
					$this->session->set_userdata('id_user', $result['id_user_account']);
					$this->md->setLoginActivity(json_encode($_POST),1, $result['id_user_account']);
					redirect($this->input->post('redirect') ? $this->input->post('redirect') : base_url().'dashboard');
					
				} else {
					$this->md->setLoginActivity(json_encode($_POST),2, $result['id_user_account']);
					$error = "User Inactive.";				
				}
			} else{
				$this->md->setLoginActivity(json_encode($_POST),0);
				$error = "Wrong username/password combination.";
			}
		}
		if($error) $this->smarty->assign('error', $error);
	}

	function logout() {
		//Destroy session
		$this->session->sess_destroy();
		redirect(BASE . 'account/login');

	}

	function forgot_password() {
		//Send temporary password to user account email
	}


// 	function index() {
// 		$session_user = $this->session->userdata('_session');
// 		if(!$session_user) redirect(BASE . 'dashboard/login');
// 		$this->load->model('core/globals_model', 'globals');
// 		if($session_user['id_user_group'] == 1) {

// 			$pending['users']['count'] = $pending['users']['data'] ? count($pending['users']['data']) : 0;
// 			$pending['projects']['data'] = $this->globals->get('project', array('status'=> 5));
// 			$pending['projects']['count'] = $pending['projects']['data'] ? count($pending['projects']['data']) : 0;

// 			$pending['projects'] = $this->globals->getProjects(false, 5);
// 			$pending['users'] = $this->globals->get('user', array('status'=> 6));
// 			$this->smarty->assign('pending', $pending);	
// 			$this->smarty->assign('locations', json_encode($this->globals->get('aux_location')));	
// 		} else {
// 			foreach($this->globals->getProjects($this->session->userdata('id_user')) as $d) {
// 				if($d['id_project_role'] == 1) $d['pending_members'] = $this->globals->getProjectMembers($d['id_project'], 'PENDING');
// 				$data[] = $d;
// 			}
// 			$this->smarty->assign('data', $data);
// 			$pending['projects'] = $this->globals->getProjects($this->session->userdata('id_user'), 5);
// 			$pending['members'] = $this->globals->getProjects($this->session->userdata('id_user'), 1, 'PENDING');
// 			$this->smarty->assign('pending', $pending);
// 		}
// 	}	
	
// 	function login() {
// 		$error = false;
// 		$this->load->library('encrypt');
// 		if($this->input->cookie(COOKIE)){
			

// 			$cookie = $this->md->getCookies($this->input->cookie(COOKIE));
// 			$cookie['data'] = json_decode($cookie['data'], true);

// 			$cookie['password'] = $this->encrypt->decode($cookie['password']);
// 			$this->smarty->assign('data', $cookie);
// 		}
		
// 		if ($_POST) {
// 			$data = $this->input->post('data');

// 			$result = $this->md->login($data);
			
// 			if($result){
// 				$data['password'] = $this->encrypt->encode($data['password']);
// 				if($this->input->post('remember') == 'yes') $this->md->setCookies($_SERVER['REMOTE_ADDR'], $data);					
// 				else setcookie(COOKIE, '', 0);
				
// 				if($result['status'] == 1) {
// 					$this->session->set_userdata('_session', $result);
// 					$this->session->set_userdata('id_user', $result['id_user_account']);
// 					$this->md->setLoginActivity(json_encode($_POST),1, $result['id_user_account']);
// 					redirect($this->input->post('redirect') ? $this->input->post('redirect') : base_url().'dashboard');
					
// 				} else {
// 					$this->md->setLoginActivity(json_encode($_POST),2, $result['id_user_account']);
// 					$error = "User Inactive.";				
// 				}
// 			} else{
// 				$this->md->setLoginActivity(json_encode($_POST),0);
// 				$error = "Wrong username/password combination.";
// 			}
// 		}
// 		if($error) $this->smarty->assign('error', $error);
		
// 	}
	
// 	function logout() {
// 		$this->session->sess_destroy();
// 		redirect(BASE . DASHBOARD . '/login');	
// 	}
	
// 	function verify($id_user, $token) {
// 		$verify = $this->md->verifyEmail($id_user, $token);
// 		if($verify) redirect(BASE . DASHBOARD . '/login');	
// 		else echo 'The token is expired or does not exist.';
// 		exit();
// 	}
// /*	
// 	function forgot_password($token) {
// 		if(!$token) show_404();
// 		$confirm = $this->manager->confirmEmail($token);
// 		$current_date = strtotime(date('Y/m/d h:i:sa')); 
// 		$expire_date = strtotime($confirm['expire']);
		
// 		$this->template->assign('curr', $current_date);
// 		$this->template->assign('expi', $expire_date);
// 		if($current_date<=$expire_date)
// 			redirect(BASE.'dashboard/change_password/'.$token);
// 		else
// 			redirect(BASE.'login');
		
// 	}
// */
// 	function forgot_password() {
		
// 		if($_POST) {
			
// 			$data = $this->md->checkEmail($this->input->post('email'));
// 			if($data) {
// 				if($this->md->sendPasswordResetEmail($data)) {
// 					$this->smarty->assign('success', true);
// 				} else {
// 					$this->smarty->assign('error','Unable to send password reset. <br />Please contact the system administrator.');
// 				}
// 			} else {
// 				$this->smarty->assign('error','Invalid email address.');
// 			}
// 		}
// 	}
	
// 	function change_password($token) {
// 		if(!$token) show_404();
// 		$confirm = $this->manager->confirmEmail($token);
// 		$user = $this->manager->checkEmail($confirm['email']);
// 		if(!$confirm || !$user) show_404();
// 		$this->template->assign('id_user', $user['id_user_account']);
// 	}
	
// 	function logs() {
	
// 	}
	
// 	function register() {
	
// 	}
	
// 	function search() {
// 		$keyword = $this->input->post('keyword');
// 		$this->smarty->assign('data', false);
// 		$this->smarty->assign('keyword', $keyword);
// 	}
	
// 	function settings() {
	
// 	}
	
	function process($action) {
		switch($action) {
			case 'register' : echo $this->md->register($this->input->post('data')); break;
			case 'forgot-password' : echo $this->md->forgotPassword($data); break;
			case 'resend-confirmation-email' : 
				$data = array(
					'email' => $this->input->post('email'),
					'id_user' => $this->input->post('id'),
				);
				echo $this->md->sendConfirmationEmail($data);
			break;
			case 'check-email' : echo $this->md->checkDuplicate($this->input->post('data'));  break;
			case 'check-username' : echo $this->md->checkDuplicate($this->input->post('data'));  break;
			case 'change-pass' : 
				if($this->input->post('new_pwd') !== $this->input->post('passconf')){
					$result = json_encode(array("status" => 0, "msg" => "Passwords do not match!"));
				}else{
					$this->load->model('users/users_model', 'user');
					$result = $this->user->change_pass($this->input->post('id_user'), $this->input->post('old_pwd'), $this->input->post('new_pwd')); 
				}
				break;			
			
			default : break;
		}
		exit(0);
	}
	
	function import($params) {
		$filename = 'users.csv';
		if(!$filename) {
			echo 'You must set the parameters!';
			exit();
		}
		// Set path to CSV file		
		$csvFile = './'.$filename;
		
		if(!file_exists($csvFile)) {
			echo 'File does not exist!';
			exit();
		}
		
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) {
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);		
		
		$csv = $line_of_text;
		
		if(empty($csv)) {
			echo 'File is empty!';
			exit();
		}

		$data = array();
		
		$this->db->trans_start();
		foreach($csv as $i) {
			if($i[0] == '') break;
			$temp = array();
			$temp['first_name'] = ucwords(strtolower($i[0]));
			$temp['last_name'] = ucwords(strtolower($i[1]));
			$temp['username'] = left($temp['first_name']) . str_replace(' ','',strtolower($temp['last_name']));
			$temp['email'] = $temp['username'] . '@gmail.com';
			$temp['password'] = sha1($temp['username']);
			$temp['status'] = 1;
			$temp['creation_date'] = date('Y-m-d H:i:s');
			$values[] = $temp;			
		}
		$this->db->insert_batch('user', $values);
		$this->db->trans_complete();
		echo $this->db->insert_id();
		return $this->db->trans_status() == TRUE ? 1 : false;
		exit();
	}
}
