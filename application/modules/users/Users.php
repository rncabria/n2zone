<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Controller {
	
	function __construct() {
		parent::__construct();		
		$this->load->model('users_model', 'md');
		$this->load->model('core/globals_model', 'globals');
		$this->load->library('encrypt');
		$this->id_user = $this->session->userdata('id_user');
	}

	function index($user_group = false) {
		if($user_group == 'admin') $filter['id_user_group'] = 1;
		elseif($user_group == 'registered') $filter['id_user_group'] = 2;
		$filter['status'] = 1;
		$status = array(
			1 => 'Active',			
			5 => 'Unverified',
			6 => 'Pending',
			3 => 'Inactive',
		);
		if($_POST) $filter = $this->input->post('filter');		
		$data = $this->md->get(false, $filter);
		$this->smarty->assign('filter', $filter);
		$this->smarty->assign('data', $data);
		$this->smarty->assign('ddl_status', $status);
		
	}
	
	function add() {
		
	}
	
	function edit($id) {
		if(!$id) show_404();
		$data = $this->md->get($id);
		if(!$data) show_404();
		$this->smarty->assign('data', $data);
	}
	
	function view($id) {
		if(!$id) show_404();
		$data = $this->md->get($id);
		if(!$data) show_404();
		$this->smarty->assign('data', $data);
	}

	function login_activity($id) {
		if(!$id) show_404();
		$data = $this->globals->get('login_activity', array('id_user'=> $id), array('date_add'=>'DESC'));
		if(!$data) show_404();
		$this->smarty->assign('user', $this->md->get($id));
		$this->smarty->assign('data', $data);
	}

	
	function account($edit = false) {
		$data = $this->md->get($this->id_user);
		$this->smarty->assign('data', $data);
		if($edit == 'edit')  $this->smarty->assign('edit', 1);
		$this->smarty->assign('header', 'My Account');
		$this->smarty->assign('projects', $this->md->getProjects($this->id_user));	
	}

	function clients($id) {
		$data = $this->md->getClientAccounts($id);
		$this->smarty->assign('data', $data);
		$this->smarty->assign('user', $this->md->get($id));
		$this->smarty->assign('ddl_clients', $this->md->getClients());
	}
	
	function process($action) {
		$data = $this->input->post('data');
		$result = false;
		switch($action) {
			case 'save' : 
				$result = $this->md->save($data);
				// echo json_encode($data);
				break;
			case 'delete' : 
				$result = $this->md->delete($this->input->post('id')); 
				break;
			case 'change-pass' : 
				if($this->input->post('new_pwd') !== $this->input->post('passconf')){
					$result = json_encode(array("status" => 0, "msg" => "Passwords do not match!"));
				}else{
					$result = $this->md->change_pass($this->input->post('id_user'), $this->input->post('old_pwd'), $this->input->post('new_pwd')); 
				}
				break;
			case 'activate' : 
				$result = $this->md->changeStatus($this->input->post('id'),$this->input->post('status')); 
				break;                
			default : break;		
		}
		echo $result;
		exit();
	}
}
