<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {
	private $tbl = 'user_acct';
    function __construct() {
		parent::__construct();
		$this->id_user = $this->session->userdata('id_user');
	}
	
	function get($id = false, $filter = false) {
		$this->db->select('a.*');
		$this->db->from($this->tbl . ' a');
		if($id) $this->db->where('id_user_account', $id);
		if($filter['status']) $this->db->where('a.status', $filter['status']);
		if($filter['id_user_group']) $this->db->where('a.id_user_group', $filter['id_user_group']);
		$query = $this->db->get();
		return $query->num_rows() ? ($id ? $query->row_array() : $query->result_array()) : false;
	}
	
	function save($data) {		
		$this->db->trans_start();
		$data['first_name'] = ucfirst(strtolower($data['first_name']));
		$data['last_name'] = ucfirst(strtolower($data['last_name']));
		$data['middle_name'] = ucfirst(strtolower($data['middle_name']));
		if($_FILES){
			$data['file'] = $this->upload_file();
		}
		
		if($data['password'] && $data['password'] == $this->input->post('passconf')){
			$data['password'] = sha1($data['password']);
		} 
		else if($data['password'] && $data['password'] != $this->input->post('passconf')){
			return false;
		}
		else if($data['password']){
			unset($data['password']);
		}
		
			//email and username availability check
		if(!isset($data['id_user_account'])) {
			$this->db->select('a.*');
			$this->db->from($this->tbl . ' a');
			if(isset($data['id_' . $this->tbl])) $this->db->where('id_user_account !=', $data['id_user_account']);
			$this->db->where('email', $data['email']);
			$this->db->where('username', $data['username']);
			$query_avail = $this->db->get();
			if($query_avail->num_rows()) return json_encode($data); 
		}
		
		if(isset($data['id_user_account'])) {
			$data['last_updated_by'] = $this->id_user;
			$data['last_update_date'] = date('Y-m-d H:i:s');
			$this->db->where('id_user_account', $data['id_user_account']);
			$this->db->update($this->tbl, $data);
		} else {
			$data['created_by'] = $this->id_user;
			$data['creation_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->tbl, $data);
		}
		$id = $data['id_user_account'] ? $data['id_user_account'] : $this->db->insert_id();
		if($this->id_user == $id) $id = 'account';
		$this->db->trans_complete();
		return $this->db->trans_status() == TRUE ? $id : false;
	}
	
	function delete($id) {
		$this->db->trans_start();
		if(strstr(',',$id)) $this->db->where_in('id_user_account', explode(',',$id));
		else $this->db->where('id_user_account', $id);
		$this->db->update($this->tbl, $data);
		$this->db->trans_complete();
		return $this->db->trans_status() == TRUE ? $id : false;
	}
    
    function changeStatus($id_user,$status) {
        $this->db->where('id_user_account',$id_user);
        return $this->db->update($this->tbl,array('status'=>$status));
    }
	
	function change_pass($id_user, $old_pwd, $new_pwd){
		$this->db->select('a.*');
		$this->db->from($this->tbl . ' a');
		$this->db->where('id_user_account', $id_user);
		$this->db->where('password', sha1($old_pwd));
		$query = $this->db->get();
		
		if($query->num_rows() != 1) return json_encode(array("status" => 0, "msg" => "Incorrect password!"));
		else if($query->num_rows() == 1){
			$this->db->trans_start();
			$this->db->where('id_user_account', $id_user);
			$this->db->where('password', sha1($old_pwd));
			$this->db->update($this->tbl, array('password' => sha1($new_pwd)));
			$this->db->trans_complete();
			return json_encode(array("status" => 1, "msg" => "Password changed!"));
		}
		return json_encode(array("status" => 0, "msg" => "Unknown error!"));
	}
	
	function upload_file(){
		$config = array(
            'upload_path'   => './uploads/user/',
			'allowed_types' => 'jpeg|jpg|png|bmp',
            'overwrite'     => 0
        );
		
		$this->load->library('upload', $config);
		$this->load->helper('string');
		$this->load->helper('file');
		
		$this->upload->initialize($config);
		
		$upload_data = false;
		
		if ($this->upload->do_upload('file')) {
			$upload_data = $this->upload->data();
		} else {
			return false;
		}
		
		return $upload_data['file_name'];
	}

	function getClientAccounts($id) {
		$this->db->select('cu.*, c.*');
		$this->db->from('user_acct_client_acct cu');
		$this->db->join('client_acct c', 'cu.id_client_account = c.id_client_acct');
		$this->db->where('cu.id_user_account', $id);
		$query = $this->db->get();
		return $query->num_rows() ? $query->result_array() : false;
	}

	function getClients() {
		$this->db->from('client_acct');
		$query = $this->db->get();
		return $query->num_rows() ? $query->result_array() : false;
	}
}