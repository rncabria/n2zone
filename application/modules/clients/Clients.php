<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Clients extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('clients_model', 'md');
	}

	function index() {
		$data = $this->md->get();
		$this->smarty->assign('data', $data);
	}

	function clients($id) {
		$data = $this->md->getAccounts($id);
		$this->smarty->assign('data', $data);
		$this->smarty->assign('client', $this->md->get($id));
		$this->smarty->assign('ddl_users', $this->md->getUsers());
	}

	function process($action) {
		$data = $this->input->post('data');
		$id = $this->input->post('id');
		switch($action) {
			case 'save' : echo $this->md->save($data); break;
			case 'get' : echo json_encode($this->md->get($id)); break;
			case 'delete' : echo $this->md->delete($id); break;
			case 'add-account' : echo $this->md->addUserAccount($data); break;
			case 'delete-account' : echo $this->md->deleteUserAccount($id); break;
			default : break;
		}
		exit(0);
	}
}
