<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sales extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('sales_model', 'md');
	}

	function index() {
		$filter['year'] = date('Y');
		$filter['month'] = date('n');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = date('Y-m-01', strtotime($filter['year'] . '-' . $filter['month'] . '-01'));
		$filter['end'] = date('Y-m-t', strtotime($filter['start']));

		$data = $this->md->get(false, $filter);
		$this->smarty->assign('data', $data);
		$this->smarty->assign('filter', $filter);
		$this->smarty->assign('total', $this->md->get(false, $filter, true));

	}

	function process($action) {
		$data = $this->input->post('data');
		switch($action) {
			case 'save' : echo $this->md->save($data); break;
			case 'get' : echo json_encode($this->md->get($this->input->post('id'))); break;
			case 'delete' : echo $this->md->delete($this->input->post('id')); break;
			default : break;
		}
		exit(0);
	}
}
