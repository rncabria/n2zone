<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_titles_model extends CI_Model {
	private $tbl = 'acct_titles';
    function __construct() {
		parent::__construct();
		$this->id_user = $this->session->userdata('id_user');
		//ejae cute
	}
	
	function get($id = false) {
		$this->db->select('a.*');
		$this->db->from($this->tbl . ' a');
		if($id) $this->db->where('id_' . $this->tbl, $id);
		$query = $this->db->get();
		return $query->num_rows() ? ($id ? $query->row_array() : $query->result_array()) : false;
	}

	function save($data) {
		$this->db->trans_start();
		if($data['id_' . $this->tbl] > 0) {
			$data['last_updated_by'] = $this->id_user;
			$data['last_update_date'] = date('Y-m-d H:i:s');
			$this->db->where('id_' . $this->tbl, $data['id_' . $this->tbl]);
			$this->db->update($this->tbl, $data);
		} else {
			$data['created_by'] = $this->id_user;
			$data['creation_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->tbl, $data);
		}
		$id = $data['id_' . $this->tbl] ? $data['id_' . $this->tbl] : $this->db->insert_id();
		$this->db->trans_complete();
		return $this->db->trans_status() == TRUE ? $id : false;
	}

	function delete($id) {
		$data['date_delete'] = date('Y-m-d H:i:s');
		$this->db->trans_start();
		if(strstr(',',$id)) $this->db->where_in('id_' . $this->tbl, explode(',',$id));
		else $this->db->where('id_' . $this->tbl, $id);
		$this->db->update($this->tbl, $data);
		$this->db->trans_complete();
		return $this->db->trans_status() == TRUE ? $id : false;
	}

	function getAccounts($id) {
		$this->db->select('cu.*, u.*');
		$this->db->from('client_acct cu');
		$this->db->join('user u', 'cu.id_user = u.id_user');
		$this->db->where('cu.id_client_acct', $id);
		$query = $this->db->get();
		return $query->num_rows() ? $query->result_array() : false;
	}

	function getUsers() {
		$this->db->where('status', 1);
		$this->db->from('user');
		$query = $this->db->get();
		return $query->num_rows() ? $query->result_array() : false;
	}

	function addUserAccount($data) {
		$data['creation_date'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata('id_user');
		$this->db->insert('client_user', $data);
		return $this->db->insert_id();
	}

	function deleteUserAccount($id) {
		$this->db->where('id_client_user', $id);
		return $this->db->delete('client_user');
	}
}