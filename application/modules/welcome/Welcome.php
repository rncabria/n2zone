<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model('Welcome_model','md');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	function smarty()
	{
		$this->smarty->debugging = true;
		$this->smarty->caching = true;
		$this->smarty->cache_lifetime = 120;

		$this->smarty->assign("Name", "Fred Irving Johnathan Bradley Peppergill", true);
		$this->smarty->assign("FirstName", array("John", "Mary", "James", "Henry"));
		$this->smarty->assign("LastName", array("Doe", "Smith", "Johnson", "Case"));
		$this->smarty->assign("Class", array(array("A", "B", "C", "D"), array("E", "F", "G", "H"),
		                               array("I", "J", "K", "L"), array("M", "N", "O", "P")));

		$this->smarty->assign("contacts", array(array("phone" => "1", "fax" => "2", "cell" => "3"),
		                                  array("phone" => "555-4444", "fax" => "555-3333", "cell" => "760-1234")));

		$this->smarty->assign("option_values", array("NY", "NE", "KS", "IA", "OK", "TX"));
		$this->smarty->assign("option_output", array("New York", "Nebraska", "Kansas", "Iowa", "Oklahoma", "Texas"));
		$this->smarty->assign("option_selected", "NE");

		$this->smarty->display('index.tpl');
	}	
}
