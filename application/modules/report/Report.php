<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('report_model', 'md');
		$this->load->model('core/globals_model', 'globals');
	}

	function index() {
		$filter['year'] = date('Y');
		$filter['month'] = date('n');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = date('Y-m-01', strtotime($filter['year'] . '-' . $filter['month'] . '-01'));
		$filter['end'] = date('Y-m-t', strtotime($filter['start']));

		$data = $this->md->get(false, $filter);
		$this->smarty->assign('data', $data);
		$this->smarty->assign('filter', $filter);
		$this->smarty->assign('total', $this->md->get(false, $filter, true));

	}	

	function sales() {
		$filter['year'] = date('Y');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = $filter['year'] . '-01-01';
		$filter['end'] = $filter['year'] . '-12-31';

		$this->smarty->assign('filter', $filter);	
	}

	function purchases() {
		$filter['year'] = date('Y');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = $filter['year'] . '-01-01';
		$filter['end'] = $filter['year'] . '-12-31';

		$this->smarty->assign('filter', $filter);	
	}

	function disbursements() {
		$filter['year'] = date('Y');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = $filter['year'] . '-01-01';
		$filter['end'] = $filter['year'] . '-12-31';

		$this->smarty->assign('filter', $filter);			
	}

	function ledger() {
		$filter['year'] = date('Y');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = $filter['year'] . '-01-01';
		$filter['end'] = $filter['year'] . '-12-31';

		$this->smarty->assign('filter', $filter);	
	}

	function income_statement() {
		$filter['year'] = date('Y');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = $filter['year'] . '-01-01';
		$filter['end'] = $filter['year'] . '-12-31';

		$this->smarty->assign('filter', $filter);		

	}

	function balance_sheet() {
		$filter['year'] = date('Y');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = $filter['year'] . '-01-01';
		$filter['end'] = $filter['year'] . '-12-31';

		$this->smarty->assign('filter', $filter);		

	}

	function cash_flow() {
		$filter['year'] = date('Y');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = $filter['year'] . '-01-01';
		$filter['end'] = $filter['year'] . '-12-31';

		$this->smarty->assign('filter', $filter);		

	}

	function working_paper() {
		$filter['year'] = date('Y');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = $filter['year'] . '-01-01';
		$filter['end'] = $filter['year'] . '-12-31';

		$this->smarty->assign('filter', $filter);
		$this->smarty->assign('chart_of_acct', $this->md->getChartOfAccounts());
	}

	function process($action) {
		switch($action) {
			case '' : break;		
			default : break;
		}
		exit(0);
	}
}
