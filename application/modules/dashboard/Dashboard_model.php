<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {
    function __construct() {
       parent::__construct();
	   
	}

	function getAccount($id) {
		$this->db->where('id_client',$id);
		$query = $this->db->get('client');
		return $query->num_rows() ? $query->row_array() : false;
	}

}