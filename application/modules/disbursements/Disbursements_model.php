<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Disbursements_model extends CI_Model {
	private $tbl = 'client_acct_disbursements';

    function __construct() {
       parent::__construct();
       $this->id_user = $this->session->userdata('id_user');
	   
	}

	function get($id = false, $filter = false, $sum = false) {
		if($sum) $this->db->select('sum(a.amount_dr) as amount_dr, sum(a.amount_cr) as amount_cr');
		else $this->db->select('a.id_'.$this->tbl.' as id, a.particulars as name, a.*');
		$this->db->from($this->tbl . ' a');
		if($id) $this->db->where('id_' . $this->tbl, $id);
		if($filter['start']) $this->db->where('date >=', $filter['start']);
		if($filter['end']) $this->db->where('date <=', $filter['end']);
		$query = $this->db->get();
		return $query->num_rows() ? ($id || $sum ? $query->row_array() : $query->result_array()) : false;
	}

	function save($data) {
		$data['id_client'] = $this->session->userdata('id_client');
		$this->db->trans_start();
		if($data['id_' . $this->tbl] > 0) {
			$data['last_updated_by'] = $this->id_user;
			$data['last_update_date'] = date('Y-m-d H:i:s');
			$this->db->where('id_' . $this->tbl, $data['id_' . $this->tbl]);
			$this->db->update($this->tbl, $data);
		} else {
			$data['created_by'] = $this->id_user;
			$data['creation_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->tbl, $data);
		}
		$id = $data['id_' . $this->tbl] ? $data['id_' . $this->tbl] : $this->db->insert_id();
		$this->db->trans_complete();
		return $this->db->trans_status() == TRUE ? $id : false;
	}

	function delete($id) {
		$this->db->trans_start();
		if(strstr(',',$id)) $this->db->where_in('id_' . $this->tbl, explode(',',$id));
		else $this->db->where('id_' . $this->tbl, $id);
		$this->db->update($this->tbl, $data);
		$this->db->trans_complete();
		return $this->db->trans_status() == TRUE ? $id : false;
	}

}