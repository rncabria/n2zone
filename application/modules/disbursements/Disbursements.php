<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Disbursements extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('disbursements_model', 'md');
	}

	function index() {
		$filter['year'] = date('Y');
		$filter['month'] = date('n');

		if($_POST) $filter = $this->input->post('filter');

		$filter['start'] = date('Y-m-01', strtotime($filter['year'] . '-' . $filter['month'] . '-01'));
		$filter['end'] = date('Y-m-t', strtotime($filter['start']));

		$input = array('cash_cr','salaries_dr','rent_dr','insurance_dr','repres_ent_dr','transp_travel_dr','comm_dr','light_water_dr','office_supplies_dr','taxes_license_dr','sss_philhealth_dr','sss_philhealth_cr','wth_tax_dr','wth_tax_cr','income_tax_dr','percent_payable_dr','sundries_dr','sundries_cr');
		$label = array('Cash Dr','Salaries Dr', 'Rent Dr', 'Insurance Dr', 'Repres & Entertainment Dr', 'Transport and Travel Dr','Communications Dr','Light and Water Dr','Office Supplies Dr','Taxes and Licenses Dr','SSS/Philhealth Payable Cr','Withholding Tax Dr', 'Withholding Tax Cr','Income Tax Dr','Income Tax Cr','% Payable Dr','Sundries Dr','Sundries Cr');
		$this->smarty->assign('input', $input);
		$this->smarty->assign('label', $label);

		$data = $this->md->get(false, $filter);
		$this->smarty->assign('data', $data);
		$this->smarty->assign('filter', $filter);
		$this->smarty->assign('total', $this->md->get(false, $filter, true));

	}

	function process($action) {
		$data = $this->input->post('data');
		switch($action) {
			case 'save' : echo $this->md->save($data); break;
			case 'get' : echo json_encode($this->md->get($this->input->post('id'))); break;
			case 'delete' : echo $this->md->delete($this->input->post('id')); break;
			default : break;
		}
		exit(0);
	}
}
