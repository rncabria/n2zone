<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ledger_model extends CI_Model {
	private $tbl = 'acct_ledger';

    function __construct() {
       parent::__construct();
	   
	}

	function get($id = false, $filter = false) {
		$this->db->select('a.*');
		$this->db->from($this->tbl . ' a');
		if($id) $this->db->where('id_' . $this->tbl, $id);
		$query = $this->db->get();
		return $query->num_rows() ? ($id ? $query->row_array() : $query->result_array()) : false;
	}

}