<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Globals_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}
	
	function clean($data) {
		if(empty($data)) return false;
		$result = array();
		foreach($data as $k=>$v) {
			$k = str_replace(_COLUMN_IDENTIFIER_, '', $k);
			$result["$k"] = is_array($v) ? json_encode($v) : $v;
		}
		$result['last_update_date'] = date('Y-m-d H:i:s');
		$result['last_updated_by'] = $this->session->userdata('id_user');
		return $result;
	}
	
	function get($table, $where = false, $order_by = false, $group_by = false, $is_row = false) {
		if($where && is_array($where)) {
			foreach($where as $k=>$v) {
				$this->db->where($k, $v);
			}
		}
		if($order_by && is_array($order_by)) {
			foreach($order_by as $k=>$v) {
				$this->db->order_by($k, $v);
			}
		}
		if($group_by && is_array($group_by)) {
			foreach($group_by as $k=>$v) {
				$this->db->group_by($k, $v);
			}
		}
		$query = $this->db->get($table);
		return $query->num_rows() ? ($is_row ? $query->row_array() : $query->result_array()) : false;
	}	
	
	function delete($table, $ids, $force_delete = false) {
		$ids = explode('',$ids);
		if(is_array($ids)) {
			$this->db->where_in('id_' . $table, $ids);
		} else {
			$this->db->where('id_' . $table, $ids);
		}
		return $this->db->update($table, array('status' => 0));
		return $this->db->delete($table);
	}

	function edit($table, $data) {
		if(!($table && $data)) return false;
		$data['last_updated_by'] = $this->session->userdata('id_user');
		$data['last_update_date'] = date('Y-m-d H:i:s');
		$this->db->where('id_' . $table, $data['id_' . $table]);
		return $this->db->update($table, $data);
	}

	function add($table, $data) {
		if(!($table && $data)) return false;
		$data['id_user_account'] = $this->session->userdata('id_user');
		$data['creation_date'] = date('Y-m-d H:i:s');
		$this->db->where('id_' . $table, $data['id_' . $table]);
		return $this->db->update($table, $data);
	}

	function isProjectMember($id_user, $id_project) {
		$this->db->where('id_user', $id_user);
		$this->db->where('id_project', $id_project);
		$query = $this->db->get('project_member');
		return $query->num_rows() ? 1 : false; 
	}

	function getProjectMembership($id_user, $id_project) {
		$this->db->select('m.*, r.name as role');
		$this->db->from('project_member m');
		$this->db->join('aux_project_role r', 'm.id_project_role = r.id_project_role');
		$this->db->where('m.id_user', $id_user);
		$this->db->where('m.id_project',$id_project);
		$query = $this->db->get();
		return $query->num_rows() ? $query->row_array() : false; 
	}
	
	function log($params) {
		$params['data'] = json_encode($params['data']);
		$this->db->insert('logs', $params);
	}
	
	function getProjects($id_user, $status = false, $member = false) {
		$this->db->select('m.*, p.*, c.code, r.name as role, l.location, m.status as role_status, c.name as category');
		$this->db->from('project_member m');
		$this->db->join('project p','m.id_project = p.id_project');
		$this->db->join('aux_project_role r','m.id_project_role = r.id_project_role','left');
		$this->db->join('aux_location l','p.id_aux_location = l.id_aux_location', 'left');
		$this->db->join('aux_research_category c','p.id_aux_research_category = c.id_aux_research_category','left');
		if($id_user) $this->db->where('m.id_user', $id_user);
		$this->db->where('p.status', $status ? $status : 1);
		$this->db->where('m.status', $member == 'PENDING' ? 0 : 1);
		$this->db->where('p.id_user_delete', 0);
		$query = $this->db->get();
		return $query->num_rows() ? $query->result_array() : false; 
		
	}
	
	function getProjectMembers($id_project, $status = false) {
		$this->db->select('pm.*, u.username, u.file');
		$this->db->from('project_member pm');
		$this->db->join('user u','pm.id_user = u.id_user');
		$this->db->where('pm.id_project', $id_project);
		$this->db->where('pm.status', $status == 'PENDING' ? 0 : 1);
		$query = $this->db->get();
		return $query->num_rows() ? $query->result_array() : false; 	
	}
	
	function getProjectAdmin($id_project) {
		$this->db->from('project_member pm');
		$this->db->join('user u','pm.id_user = u.id_user');
		$this->db->where('id_project', $id_project);
		$this->db->where('id_project_role', 1);
		$this->db->where('pm.status', 1);
		$query = $this->db->get();
		return $query->num_rows() ? $query->row_array() : false;
		
	}
	
	function getMarkers($id_project) {
		$this->db->where('id_project', $id_project);
		$query = $this->db->get('map');
		return $query->num_rows() ? $query->result_array() : false; 	
	}
}