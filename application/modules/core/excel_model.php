<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'excel/PHPExcel.php';
class Excel_model extends CI_Model {

	private $objPHPExcel;
	private $excel;
	
    function __construct() {
		parent::__construct();
		
		// Create new PHPExcel object
		$this->objPHPExcel = new PHPExcel();
		$this->objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
		$this->objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
		// Set document properties
		$this->objPHPExcel->getProperties()->setCreator("")->setLastModifiedBy("")->setTitle("")->setSubject("")->setDescription("")->setKeywords("report")->setCategory("");
		$this->excel = $this->objPHPExcel->setActiveSheetIndex(0);
		$this->sheet = $this->objPHPExcel->getActiveSheet();
		$this->pageMargins = $this->sheet->getPageMargins();

		// margin is set in inches (0.5cm)
		$this->margin = 0.5 / 2.54;
		$this->pageMargins->setTop(2 / 2.54);
		$this->pageMargins->setBottom($this->margin);
		$this->pageMargins->setLeft(2 / 2.54);
		$this->pageMargins->setRight($this->margin);
		
		$this->objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
		//$this->objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
		$this->objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
		$this->objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
		$this->objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);		
	}
	
	function export($params, $data, $header, $fields) {	
		$this->excel->getStyle('A1:S6')->getFont()->setBold(true);
		$this->excel->getStyle('A1:S6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);		
		$this->excel->mergeCells('A1:I1')->setCellValue('A1', $params['title'])->getStyle('A1')->getFont()->setSize(16);
		$this->excel->mergeCells('A2:I2')->setCellValue('A2', date('Y-m-d H:i:s'))->getStyle('A2')->getFont()->setSize(10);
	
		$this->excel->setTitle($params['title']);
		$this->objPHPExcel->setActiveSheetIndex(0);

		// redirect output to client browser 
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');		
		header('Content-Disposition: attachment;filename="' . $params['filename']  . '.xlsx"'); 
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		exit(0);	
	}

function excelReader($filename) {
		$inputFileName = './import/'. $filename;
		//  Read your Excel workbook
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}
		$this->db->trans_start();

		$id_set = 1;
		$objPHPExcel->setActiveSheetIndex(0);
		$sheet = $objPHPExcel->getActiveSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		$fields = $objPHPExcel->setActiveSheetIndex(0)->rangeToArray('A1:' . $highestColumn . '1');		
		$data = $objPHPExcel->setActiveSheetIndex(0)->rangeToArray('A2:' . $highestColumn . $highestRow);

		$values = array();
		foreach($data as $d) {
			$v = array();
			$v['status'] = 1;
			$v['id_user_group'] = 2;
			$v['creation_date'] = date('Y-m-d H:i:s');
			$v['created_by'] = 1;
			foreach($fields[0] as $k=>$f) {
				if($f == '') continue;
				$v["$f"] = $d[$k];
			}
			$values[] = $v;
		}
		var_dump($values);
		$this->db->insert_batch('user', $values);
		exit();
	}	
}