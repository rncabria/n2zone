<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Vii Framework
 *
 * @package			ViiFramework (libraries from CodeIgniter)
 * @author			ViiWorks Production Team
 * @copyright		Copyright (c) 2009 - 2011, ViiWorks Inc.
 * @website url 	http://www.viiworks.com/
 * @filesource
 *
 */
 
class S3_model extends CI_Model
{
    function __construct()
    {
       parent::__construct();
		if (!class_exists('S3')) require_once '../../libraries/S3.php';
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAIG2WVOQXRMLCC73A');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		$this->s3 = new S3(awsAccessKey, awsSecretKey);
	}
	
	function uploadFile($source,$bucketName,$destination){
		if($this->s3->putObjectFile($source, $bucketName, $destination, S3::ACL_PUBLIC_READ_WRITE))
			return true;
		return false;
	}
	
	function isExist($bucketName,$fileName){
		if($this->s3->getObjectInfo($bucketName, $fileName))
			return true;
		return false;
	}
	
	function deleteFile($bucketName,$uri){
		return $this->s3->deleteObject($bucketName,($uri));
	}
}
?>