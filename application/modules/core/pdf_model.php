<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("dompdf/dompdf_config.inc.php");
spl_autoload_register('DOMPDF_autoload');

class Pdf_model extends CI_Model {
	
	function __construct() {
		parent::__construct();		
	}
		
	function showPDF($params) {
		$dompdf = new DOMPDF();
		$html = $this->smarty->fetch('templates/dashboard/pdf/'.$params['template'].'.html');
		
		$dompdf->load_html($html);
		$dompdf->set_paper( array(0,0, 8.5 * 72, 11 * 72), "portrait" );
		$dompdf->render();
		$dompdf->stream($params['filename']. ".pdf", array('Attachment'=>0));
		exit(0);
	}
	
	function landscape($params) {
		$dompdf = new DOMPDF();
		$html = $this->smarty->fetch('templates/dashboard/pdf/'.$params['template'].'.html');
		
		$dompdf->load_html($html);
		$dompdf->set_paper( 'letter', "landscape" );
		$dompdf->render();
		$dompdf->stream($params['filename']. ".pdf", array('Attachment'=>0));
		exit(0);
	}
}