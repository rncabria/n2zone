<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Vii Framework
 *
 * @package			ViiFramework (libraries from CodeIgniter)
 * @author			ViiWorks Production Team
 * @copyright		Copyright (c) 2009 - 2011, ViiWorks Inc.
 * @website url 	http://www.viiworks.com/
 * @filesource
 *
 */
 
class Cfs_model extends CI_Model
{
	private $table = 'report_cfs';
    function __construct()
    {
		parent::__construct();
		$this->id_pawnshop = $this->session->userdata('id_pawnshop');
		$this->id_user = $this->session->userdata('id_user');
		$this->id_branch = $this->session->userdata('id_branch');
		$this->load->model('core/dbtm_model','dbtm');
	}
	
	function getEndingCashBalance($date, $id_branch = false, $book = 'A') {
		if($id_branch) $this->db->select('SUM(' . $book . '_ending_cash_balance) as amount');
		else $this->db->select($book . '_ending_cash_balance as amount');
		$this->db->where('date', $date);
		if($id_branch) $this->db->where('id_branch', $id_branch);
		$this->db->where('id_pawnshop', $this->id_pawnshop);
		return floatval($this->db->get('report_cfs')->row()->amount);	
	}
	
	/**
	* INFLOWS
	*
	*
	*/
	function inflows($start_date, $end_date = false, $id_branch = false, $book = 'A') {
		//$inflows['beginning_cash_balance']['amount'] = $this->getBeginningCashBalance($start_date, $id_branch);
		
		//ADDITIONAL FUNDS
		$inflows['add_fund']['label'] = 'Fund Transfer IN';
		$inflows['add_fund']['amount'] = $this->computeAdditionalFund(1, $start_date, $end_date, $id_branch);
		
		//REDEEMED LOANS
		$inflows['redeem_jewelry']['label'] = 'Redeemed Loans (Jewelry)';
		$inflows['redeem_jewelry']['amount'] = $this->computePledgeLoans($start_date, $end_date, $id_branch, $book, 0, 'REDEEM');
		
		$inflows['redeem_non_jewelry']['label'] = 'Redeemed Loans (Non-Jewelry)';
		$inflows['redeem_non_jewelry']['amount'] = $this->computePledgeLoans($start_date, $end_date, $id_branch, $book, 1,'REDEEM');
		
		
		//PAWN LOANS CHARGES
		// $inflows['advance_interest']['label'] = 'Advance Interests';
		// $inflows['advance_interest']['amount'] = $this->computeCharges('advance_interest', $start_date, $end_date, $id_branch, $book);
		
		$inflows['penalty']['label'] = 'Loan Penalty';
		$inflows['penalty']['amount'] = $this->computeCharges('penalty', $start_date, $end_date, $id_branch, $book);
		
		$inflows['interest']['label'] = 'Interests';
		$inflows['interest']['amount'] = $this->computeCharges('interest', $start_date, $end_date, $id_branch, $book) + $this->computeCharges('advance_interest', $start_date, $end_date, $id_branch, $book);
		
		$inflows['service_charge']['label'] = 'Service Charge';
		$inflows['service_charge']['amount'] = $this->computeCharges('service_charge', $start_date, $end_date, $id_branch, $book);
		
		$dst = $this->computeCharges('dst', $start_date, $end_date, $id_branch, $book);
		if($dst > 0) {
			$inflows['dst']['label'] = 'DST';
			$inflows['dst']['amount'] = $dst;		
		}

		$surcharge = $this->computeCharges('surcharge', $start_date, $end_date, $id_branch, $book);
		if($surcharge > 0) {
			$inflows['surcharge']['label'] = 'Surcharge';
			$inflows['surcharge']['amount'] = $surcharge;		
		}
		
		$inflows['oc_total']['label'] = 'Other Charges';
		$inflows['oc_total']['amount'] = $this->computeCharges('oc_total', $start_date, $end_date, $id_branch, $book);
		
		$oc = $this->getOtherChargesByType($start_date, $end_date, $id_branch, $book);
		
		// foreach($this->dbtm->getter('aux_charges') as $item) {
			// $index = $item['code'];
			// if($index == 'others') {
				// foreach($oc['others'] as $key=>$i) {
					// $inflows['oc_total']['sub']["$key"]['label'] = $i['label_export'];
					// $inflows['oc_total']['sub']["$key"]['label_export'] = $i['label_export'];
					// $inflows['oc_total']['sub']["$key"]['amount'] = $i['amount'];
				// }
			// } else {
				// $inflows['oc_total']['sub']["$index"]['label'] = $item['name'];// . ' <span style="font-weight:normal">(<i>' . $item['reference'] . '</i>)</span>';
				// $inflows['oc_total']['sub']["$index"]['label_export'] = $item['name'];// . ' (' . $item['reference'] . ')';
				// $inflows['oc_total']['sub']["$index"]['amount'] = $oc["$index"];
			// }
		// }		
		
		// $inflows['security_deposit']['label'] = 'Security Deposit';
		// $inflows['security_deposit']['amount'] = $this->computeSecurityDepositTotal($start_date, $end_date, $id_branch, $book, 'new');
		
		
		//OTHER SERVICES
		// $inflows['insurance']['label'] = 'Micro Insurance';
		// $inflows['insurance']['amount'] = $this->computeTransactionTotal('insurance', $start_date, $end_date, $id_branch);
				
		// $inflows['ecpay']['label'] = 'ECPAY';
		// $inflows['ecpay']['amount'] = $this->computeTransactionTotal('ecpay', $start_date, $end_date, $id_branch);

		// $inflows['eload']['label'] = 'ECPAY E-Load';
		// $inflows['eload']['amount'] = $this->computeTransactionTotal('eload', $start_date, $end_date, $id_branch);

		// $inflows['bp']['label'] = 'ECPAY Bills Payment';
		// $inflows['bp']['amount'] = $this->computeTransactionTotal('bills_payment', $start_date, $end_date, $id_branch);

		// $inflows['at']['label'] = 'Airlines Ticketing';
		// $inflows['at']['amount'] = $this->computeTransactionTotal('airlines_ticketing', $start_date, $end_date, $id_branch);
		
		$inflows['sales']['label'] = 'Auction Sales';
		$inflows['sales']['amount'] = $this->computeAuctionSales($start_date, $end_date, $id_branch);
		
		// $inflows['scrap']['label'] = 'Scrap Sold';
		// $inflows['scrap']['amount'] = $this->computeTransactionTotal('scrap', $start_date, $end_date, $id_branch);
		

		// $inflows['sendout']['label'] = 'Western Union Sendout';
		// $inflows['sendout']['amount'] = $this->computeSendout($start_date, $end_date, $id_branch);//$this->computeTransactionTotal('sales', $start_date, $end_date, $id_branch);
		

		return $inflows;
	}
	
	function outflows($start_date, $end_date = false, $id_branch = false, $book = 'A') {
		//ADDITIONAL FUNDS
		$outflows['add_fund']['label'] = 'Fund Transfer OUT';
		$outflows['add_fund']['amount'] = $this->computeAdditionalFund(0, $start_date, $end_date, $id_branch);			
	
		$outflows['new_jewelry']['label'] = 'New Loans (Jewelry)';
		$outflows['new_jewelry']['amount'] = $this->computePledgeLoans($start_date, $end_date, $id_branch, $book, 0,  'NEW');
		
		$outflows['new_non_jewelry']['label'] = 'New Loans (Non-Jewelry)';
		$outflows['new_non_jewelry']['amount'] = $this->computePledgeLoans($start_date, $end_date, $id_branch, $book, 1,'NEW');
		
		// $outflows['purchase']['label'] = 'Purchased Items';
		// $outflows['purchase']['amount'] = $this->computeTransactionTotal('purchase', $start_date, $end_date, $id_branch);
		
		// $outflows['forex']['label'] = 'Forex Trading (BUYING)';
		// $outflows['forex']['amount'] = $this->computeForexBuying('forex', $start_date, $end_date, $id_branch);
		
		// $outflows['payout']['label'] = 'Western Union Payout';
		// $outflows['payout']['amount'] = $this->computeWesternUnion($start_date, $end_date, $id_branch, 1, true);
		
			// $outflows['payout']['sub']['peso']['label'] = 'Peso Amount';
			// $outflows['payout']['sub']['peso']['amount'] = $this->computeWesternUnion($start_date, $end_date, $id_branch, 1, false, false);//$this->computeTransactionTotal('sales', $start_date, $end_date, $id_branch);		
			
			// $outflows['payout']['sub']['forex']['label'] = 'FOREX';
			// $outflows['payout']['sub']['forex']['amount'] = $this->computeWesternUnion($start_date, $end_date, $id_branch, 1, false, true);
		
		// $outflows['expenses']['label'] = 'Expenses';
		// $outflows['expenses']['amount'] = $this->computeExpenses($start_date, $end_date, $id_branch);
		// foreach($this->dbtm->getter('aux_expense') as $item) {
			// $index = $item['code'];
			// $outflows['expenses']['sub']["$index"]['label'] = $item['name'];
			// $outflows['expenses']['sub']["$index"]['amount'] = $this->computeExpenses($start_date, $end_date, $id_branch, $index);			
		// }
		
		// $outflows['security_deposit']['label'] = 'Returned Deposit';
		// $outflows['security_deposit']['amount'] = $this->computeSecurityDepositTotal($start_date, $end_date, $id_branch, $book, 'redeem');
		
		// $outflows['salary']['label'] = 'Salaries and Wages';
		// $outflows['salary']['amount'] = $this->computeSalary($start_date, $end_date, $id_branch);
		// $outflows['discounts']['label'] = 'Discounts';
		// $outflows['discounts']['amount'] = $this->computeDiscounts($start_date, $end_date, $id_branch);

		
		return $outflows;
	
	}
	
	function computeTotalAmount($data) {
		$total = 0;
		foreach($data as $item) {
			$total += $item['amount'];
		}
		return floatval($total);
	}	

	// ============================================================================================================================
	//                                        ADDITIONAL CASH
	// ============================================================================================================================
	function computeAdditionalFund($in = false, $start_date, $end_date, $id_branch = false) {
		$this->db->flush_cache();
		$this->db->select('SUM(amount) as amount');
		$this->db->from($in ? 'funds' : 'cashout');
		if($end_date) {
			$this->db->where('date >=', $start_date);
			$this->db->where('date <=', $end_date);		
		} else {
			$this->db->where('date', $start_date);		
		}
		if($id_branch) $this->db->where('id_branch', $id_branch);
		$this->db->where('id_pawnshop', $this->id_pawnshop);
		$query =  $this->db->get();
		return $query->num_rows() ? floatval($query->row()->amount) : 0;
	}

	// ============================================================================================================================
	//                                        EXPENSES
	// ============================================================================================================================	
	function computeExpenses($start_date, $end_date, $id_branch = false, $type = false) {
		$this->db->flush_cache();
		$this->db->select('SUM(amount) as amount');
		$this->db->from('expenses');
		if($end_date) {
			$this->db->where('date >=', $start_date);
			$this->db->where('date <=', $end_date);		
		} else {
			$this->db->where('date', $start_date);		
		}
		if($id_branch) $this->db->where('id_branch', $id_branch);
		$this->db->where('id_pawnshop', $this->id_pawnshop);
		if($type) $this->db->where('type', $type);
		$query =  $this->db->get();
		return $query->num_rows() ? floatval($query->row()->amount) : 0;
	}	
	
	function computeDiscounts($start_date, $end_date, $id_branch = false, $type = false) {
		$this->db->flush_cache();
		$this->db->select('SUM(discount) as amount'); 
		$this->db->from('transaction');
		if($end_date) {
			$this->db->where('transaction_date >=', $start_date);
			$this->db->where('transaction_date <=', $end_date);		
		} else {
			$this->db->where('transaction_date', $start_date);		
		}
		if($id_branch) $this->db->where('id_branch', $id_branch);
		$this->db->where('id_pawnshop', $this->id_pawnshop);
		if($type) $this->db->where('transaction_type', $type);
		$query =  $this->db->get();
		return $query->num_rows() ? floatval($query->row()->amount) : 0;
	}	
	
	// ============================================================================================================================
	//                                        PAWN LOANS CHARGES
	// ============================================================================================================================	
	function computeCharges($charge, $start_date, $end_date, $id_branch = false, $book = 'A') {
		$this->db->select('sum(' . $charge . ') as amount');
		$this->db->from('transaction t');
		$this->db->join('inventory p', 'p.id_inventory = t.id_inventory');
		//$this->db->where('t.book', $book);
		if($id_branch) $this->db->where('t.id_branch', $id_branch);
		$this->db->where('t.id_pawnshop', $this->id_pawnshop);
		if($end_date) {
			$this->db->where('t.transaction_date >=', $start_date);
			$this->db->where('t.transaction_date <=', $end_date);		
		} else {
			$this->db->where('t.transaction_date', $start_date);
		}
		
		$query = $this->db->get();
		return $query->num_rows() ? floatval($query->row()->amount) : 0;
	}
	
	function getOtherChargesByType($start_date, $end_date, $id_branch = false, $book = 'A') {
		$this->db->select('oc_breakdown');
		$this->db->from('inventory p');
		$this->db->join('transaction t', 'p.id_inventory = t.id_inventory');
		//$this->db->where('t.book', $book);
		if($id_branch) $this->db->where('t.id_branch', $id_branch);
		$this->db->where('t.id_pawnshop', $this->id_pawnshop);
		
		if($end_date) {
			$this->db->where('t.transaction_date >=', $start_date);
			$this->db->where('t.transaction_date <=', $end_date);		
		} else {
			$this->db->where('t.transaction_date', $start_date);
		}
		
		$query = $this->db->get();
		$breakdowns = $query->num_rows() ? $query->result_array() : false;
		if(!$breakdowns) return false;
		
		$oc = array();
		// foreach($this->dbtm->getter('aux_charges') as $item) {
			// $index = $item['code'];
			// if($index == 'others') $oc["$index"] = array();
			// else $oc["$index"] = 0;
		// }
		
		foreach($breakdowns as $item) {
			$temp = json_decode($item['oc_breakdown'], true);
			foreach($temp as $i) {
				
				if($i['name'] == 'others') {
					$label = str_replace(' ', '_', strtolower($i['others']));
					$oc['others']["$label"]['label_export'] = $i['others'];
					$oc['others']["$label"]['amount'] += $i['amount'];
					
				} else {
					$oc["$type"] += $i['amount'];
				}
				
			}
		}
		return $oc;
	}	
	
	// ============================================================================================================================
	//                                        OTHER SERVICES
	// ============================================================================================================================	
	function computeTransactionTotal($module, $start_date, $end_date, $id_branch = false, $isBuying = false) {
		$this->db->select('sum(net_proceeds) as amount');
		$this->db->from('transaction t');
		if($id_branch) $this->db->where('t.id_branch', $id_branch);
		$this->db->where('t.id_pawnshop', $this->id_pawnshop);
		if($end_date) {
			$this->db->where('t.transaction_date >=', $start_date);
			$this->db->where('t.transaction_date <=', $end_date);		
		} else {
			$this->db->where('t.transaction_date', $start_date);
		}
		$this->db->where('t.transaction_type', $module);
		if($isBuying) $this->db->where('t.transaction_type', $module);
		
		$query = $this->db->get();
		return $query->num_rows() ? floatval($query->row()->amount) : 0;
	}
	
	// ============================================================================================================================
	//                                        AUCTION SALES
	// ============================================================================================================================	
	function computeAuctionSales($start_date, $end_date, $id_branch = false) {
		$this->db->select('sum(cost) as amount');
		$this->db->from('auction_sales t');
		if($id_branch) $this->db->where('t.id_branch', $id_branch);
		$this->db->where('t.id_pawnshop', $this->id_pawnshop);
		if($end_date) {
			$this->db->where('t.transaction_date >=', $start_date);
			$this->db->where('t.transaction_date <=', $end_date);		
		} else {
			$this->db->where('t.transaction_date', $start_date);
		}
		$this->db->where('t.is_deleted', 0);
		$query = $this->db->get();
		return $query->num_rows() ? floatval($query->row()->amount) : 0;
	}
	
	// ============================================================================================================================
	//                                        PLEDGE LOANS
	// ============================================================================================================================		
	function computePledgeLoans($start_date, $end_date, $id_branch = false, $book = 'A', $category, $type) {
		$this->db->flush_cache();		
		$this->db->select('SUM(p.principal) as amount');
		$this->db->from('inventory p');
		$this->db->join('transaction t', 't.id_inventory = p.id_inventory');
		$this->db->where('t.transaction_type', $type);
		//if($book) $this->db->where('t.book', $book);
		$this->db->where('p.id_item_category', $category);
		if($id_branch) $this->db->where('t.id_branch', $id_branch);
		$this->db->where('t.id_pawnshop', $this->id_pawnshop);
		if($end_date) {
			$this->db->where('t.transaction_date >=', $start_date);
			$this->db->where('t.transaction_date <=', $end_date);		
		} else {
			$this->db->where('t.transaction_date', $start_date);
		}
		$this->db->where('t.is_deleted', 0);
		$query =  $this->db->get();
		return $query->num_rows() ? floatval($query->row()->amount) : 0;
	}	

	function getCFS($date, $id_branch = false) {		
		$this->db->where('date', $date);
		$this->db->where('id_branch', $id_branch);
		$query = $this->db->get('report_cfs');
		return $query->num_rows() ? $query->row_array() : false;
	}	
	
	function saveCFS($data, $filter) {
		$values = array();
		$book = $filter['book'];
		$daterange = array_map('trim', explode('-', $filter['date']));
		$date = date('Y-m-d', strtotime($daterange[0]));
		foreach($data as $k=>$v) {
			$values[$book . '_' . $k] = $v;
		}
		$cash = array_map(function($i) {
			return str_replace(',','',$i);
		}, $this->input->post('cash'));
		$values[$book . '_cash_count'] = json_encode($cash);
		$values['last_update_date'] = date('Y-m-d H:i:s');
		$values['last_updated_by'] = $this->session->userdata('id_user');
		$this->db->where('date', $date);
		$this->db->where('id_branch', $filter['id_branch']);
		$update=  $this->db->update('report_cfs', $values);
		if($update && ($date < date('Y-m-d'))) $this->rollback_cfs($date, $filter['id_branch'], $book);
		return $update;			
	}	
	
	function rollback_cfs($date, $id_branch, $book = 'A') {
		$this->db->where('date', $date);
		$this->db->where('id_branch', $id_branch);
		$query = $this->db->get('report_cfs');
		$row = $query->num_rows() ? $query->row_array() : false;
		
		$values = array();
		$values[$book . '_beginning_cash_balance'] = $this->getEndingCashBalance(date('Y-m-d', strtotime($date . "-1 day")), $id_branch, $book);
		$values[$book .'_total_inflows'] = $this->computeTotalAmount($this->inflows($date, false, $id_branch, $book));
		$values[$book .'_total_outflows'] = $this->computeTotalAmount($this->outflows($date, false, $id_branch, $book));
		$values[$book .'_ending_cash_balance'] = $values[$book . '_beginning_cash_balance'] + $values[$book .'_total_inflows'] - $values[$book .'_total_outflows'];		
		
		if($row) {			
			$this->db->where('date', $date);
			$this->db->where('id_branch', $id_branch);
			$this->db->update('report_cfs', $values);			
		} else {
			$this->db->insert('report_cfs', $values);
		}
		
		if($date == date('Y-m-d')) return;
		$forwarded =  $values[$book .'_ending_cash_balance'];

		foreach($this->rollback_dates('cfs', $date, $id_branch) as $item) {
			$values = array();
			$values[$book . '_beginning_cash_balance'] = $forwarded;
			$values[$book .'_total_inflows'] = $this->computeTotalAmount($this->inflows($item['date'], false, $id_branch, $book));
			$values[$book .'_total_outflows'] = $this->computeTotalAmount($this->outflows($item['date'], false, $id_branch, $book));
			$values[$book .'_ending_cash_balance'] = $forwarded + $values[$book .'_total_inflows'] - $values[$book .'_total_outflows'];
			
			$this->db->where('date', $item['date']);
			$this->db->where('id_branch', $id_branch);
			$this->db->update('report_cfs', $values);
			
			$forwarded = $values[$book .'_ending_cash_balance'];
		}
	}	
	
	function rollback_dates($report, $date, $id_branch) {
		$this->db->where('date >', $date);
		$this->db->where('date <=', date('Y-m-d'));
		$this->db->where('id_branch', $id_branch);
		$this->db->order_by('date', 'ASC');
		$query = $this->db->get('report_' . $report);
		return $query->num_rows() == 0 ? false : $query->result_array();
	}	
}