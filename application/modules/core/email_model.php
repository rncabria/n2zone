<?php
class Email_model extends CI_Model
{
    function __construct()

    {
        parent::__construct();
		$this->load->database();
		$this->load->model('core/config_model','config_model');
		
    }
	/**
		Use this function to call for general email sending	
		@ $from = email of sender, <blank> or "default" for Administrators email
		@ $fromName = email of sender, <blank> or "default" for Administrators name, "site_name" for Site Name
		@ $template = HTML template
		@ $data = array of variable(s) fetched on template
	**/
	function sendEmail($to, $subject = '', $content = '', $attachment = false, $template = false, $data = false){
		$this->load->library('encrypt');
		$fromEmail = $this->config_model->get('ADMIN_EMAIL');
		$fromEmailPass = $this->config_model->get('ADMIN_EMAIL_PASS');
		$smtpHost = $this->config_model->get('ADMIN_SMTP_HOST');
		$smtpPort = $this->config_model->get('ADMIN_SMTP_PORT');
		// $fromEmail = 'admin@zycure.com';
		// $fromEmailPass = 'E4RVFVhBtkGq';
		// $smtpHost = 'mail.zycure.com';
		// $smtpPort = '2525';
		
		$siteName = $this->config_model->get('SITE_NAME');
		//$fromEmailPass =  $this->encrypt->decode($fromEmailPass);
		$config = Array(
			'protocol' => "smtp",
			'smtp_host' => $smtpHost,
			'smtp_port' => $smtpPort,
			'smtp_user' => $fromEmail,
			'smtp_pass' => $fromEmailPass,
			'charset'  => "utf-8",
			'mailtype'  => "html",
			'newline'  => "\r\n",
			'newline'  => "\r\n",
			'crlf'  => "\r\n"
		);
		$this->load->library('email',$config);	
		$this->email->from($fromEmail, $siteName);
		$this->email->reply_to($fromEmail, $siteName);	
		// $this->email->to('ruthann.cabria@viiworks.com');
		$this->email->to($to);
		if($this->config_model->get('ADMIN_BCC'))
			$this->email->bcc($this->config_model->get('ADMIN_BCC'));
		$this->email->subject($subject);	
		if($attachment){
			foreach($attachment as $item){
				$this->email->attach("././upload/documents/applicantion_forms/".$item['app_form']);
			}
		}			
		if($data)
			$this->template->assign('data',$data);
		if($template){
			$body = $this->template->fetch('email_templates/'.$template);
			$this->email->message($body);
		}else{
			$this->email->message($content);
		}
		
		
		if($this->email->send()){
			return 1;
		}
		// else return 2;
		
		// $this->email->send();
		return $this->email->print_debugger();
	}
	
	
	function prepareEmailBody($body, $data){
		foreach($data as $key => $value){	
			while(strpos($body,$key)){
				$var = strpos($body,$key);
				$body = substr_replace($body, $value, $var, strlen($key));		
			}
		}
		return $body;
	}
}
?>