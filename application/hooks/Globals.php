<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Globals {
	var $CI;
	var $module;
	var $method;
	
    function __construct() {
        $this->CI =& get_instance();		
		$this->module = CI::$APP->router->fetch_module();
		$this->method = CI::$APP->router->fetch_method();
    }

	function check_session() {
		if($this->module == 'account' || $this->module =='cron') {

		} else if (!$this->CI->session->userdata('id_user')) {
			redirect(BASE . 'account/login');
		}
		
		//GENERATE LOGS
		// if($_POST) {
		// 	$values = array(
		// 		'date_add' => date('Y-m-d H:i:s'),
		// 		'ip' => $_SERVER['REMOTE_ADDR'],
		// 		'page_url' => $_SERVER['REQUEST_URI'],
		// 		'module' => $this->module,
		// 		'method' => $this->method,
		// 		'data' => json_encode($_POST),
		// 		'id_user' => $this->CI->session->userdata('id_user'),
		// 	);
		// 	$this->CI->db->insert('logs', $values);				
		// }		
	}
	
	function display() {
		if($this->CI->session->userdata('id_user') > 0)
			$user = $this->CI->db->get_where('user_acct', array('id_user_account' => $this->CI->session->userdata('id_user')))->row_array();

		$globals = array(
			'today' => date('Y-m-d H:i:s'),
			'base' => BASE,
			'site_name' => SITENAME,
			'logo' => LOGO,
			'user' => $user,
			'module' => $this->module,
			'method' => $this->method,
			'account' => $this->CI->session->userdata('account'),
		);

		$this->CI->smarty->assign('_GLOBALS', $globals);

		$this->CI->smarty->assign('_today', date('Y-m-d'));
		$this->CI->smarty->assign('_theme', THEME);
		$this->CI->smarty->assign('_base', BASE);
		$this->CI->smarty->assign('_sitename', SITENAME);
		$this->CI->smarty->assign('_module', $this->module);
		$this->CI->smarty->assign('_method', $this->method);

		//CUSTOM GLOBAL VARIABLES
		$this->CI->smarty->assign('ddl_client', $this->CI->db->get('client')->result_array());

		if($this->method == 'login' || $this->method == 'register' || $this->method == 'forgot_password') {
			$this->CI->smarty->display('template.' . $this->method . '.html');
		
		} else if($this->module == 'error') {
			$this->CI->smarty->display('template.404.html');
		
		} else {
			if($this->method == 'add' || $this->method == 'edit') $template = 'ae';
			else $template = $this->method;
			if(file_exists(FCPATH . 'templates/modules/'.$this->module.'/' . $template . '.html'))
				$this->CI->smarty->assign('_template','modules/'.$this->module.'/' . $template . '.html');

			$this->CI->smarty->display('template.main.html');
		}

	}
}


/* End of file Globals.php */
/* Location: ./application/hooks/Globals.php */