$(document).ready(function(){
	if($('.dtable').length > 0)
		 $('.dtable').dataTable({
			responsive: true,
			iDisplayLength : 50,
			pageLength: 50
		});
		
	   $('.file-box').each(function() {
			animationHover(this, 'pulse');
		})

	if($('.form-horizontal').length > 0)
	$('.form-horizontal').validate({ errorPlacement: function (error, element) { element.before(error); }, });
	
    if($('.chosen').length > 0)
        $('.chosen').chosen();

	if($('.datepicker').length > 0) 
		$('.datepicker').datepicker({
			todayBtn: "linked",
			format : "yyyy-mm-dd",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			autoclose: true			
		});

	$('.add').on('click', function(data) {
		$('#modal-action').html('ADD');
		$('.modal form')[0].reset();
		$('#form-id').val(0)
		$('.modal').show();
	});

	$('.save').on('click', function() {
		var btn = $(this);
		var form = $(this).closest('.modal').length > 0 ? $(this).closest('.modal').find('form') : $('.form-horizontal');

		if(form.valid()) {		
			$('.form-footer btn').hide();
			$('.sk-spinner').show();				
				
			var action = btn.data('action') || 'save';			
			
			btn.prop('disabled', true);
			$('.alert').remove();
			if($('.summernote').length > 0) $('.summernote').val($('.summernote').code());//SUMMERNOTES
			$.ajax({
				url: process + action,
				type: 'POST',
				data: new FormData(form[0]),
				// data: { data : new FormData(form[0]), files : new FormData(form[1])},
				async: true,
				success: function (data) {
					if(data) {
						toastr.success('','Save successful!');
						setTimeout(function() {
							location.reload();
						}, 1500);
						// window.location.href = module_view + data;
					} else {
						toastr.error('','Error saving to database');
					}
				},
				cache: false,
				contentType: false,
				processData: false
			});
			btn.prop('disabled', false);
			$('.form-footer btn').show();
			$('.sk-spinner').hide();
			return false;
		} else {
			return false;
		}
	});

	$('.edit').on('click', function() {
		$('#modal-action').html('UPDATE');
		$('.modal form')[0].reset();
		$.post(process + 'get', {id : $(this).data('id')}).done(function(data){
			//console.log(data);
			var json = $.parseJSON(data);
			//ITERATE EACH KEY
			$.each(json, function(index,value) {
				console.log(index +': ' + value);
				$('.modal input[name="data['+index+']"]').val(value);
				$('.modal select[name="data['+index+']"]').val(value);
			});
			$('.modal').modal('show');
		});
	});
	
	$(document).on('click','.delete', function() {
		var id = $(this).data('id');
		var action = $(this).data('action') || 'delete';
		if(confirm('Are you sure you want to delete this item? ' + $(this).data('label'))) {
			
			$.post(process + action, {id : id}, function (data) {
				if(data) {
					toastr.success('','Delete Successful!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to delete item.');
				}
			});
				
		}
	});

	$('#change-account').on('change', function() {
		$.post(base + 'dashboard/process/change-account', {id : $(this).val() }).done(function(data) {
			if(data) location.reload();
		});
	});
	
	
	$(document).ajaxStop(function() {
		setTimeout($.unblockUI, 1500);
	});	
	
	$('.del').on('click', function() {
		$.blockUI({ 
			css: { 
				border: 'none', 
				padding: '15px', 
				backgroundColor: '#000', 
				'-webkit-border-radius': '10px', 
				'-moz-border-radius': '10px', 
				opacity: .5, 
				color: '#fff' 
			},
			message : 'Processing your request . . .',
		});
		var id = $(this).data('id');
		$.post(process + 'delete', {id : id}, function(data) {
			$.unblockUI;
			if(data){
				$.fn.showNotification('success','Delete Succesful!');
			} else {
				$.fn.showNotification('error','Something went wrong. Please try again.');
			}
		});
		setTimeout(function() {
			location.reload();
		}, 1500);
	});
	
	$.fn.showNotification = function(type, msg) {
		if(!msg) {
			switch(type) {
				case 'success' : msg = 'Transaction successful!'; break;
				case 'error' : msg = 'Something went wrong. Please try again.'; break;
			}
		}
		alert(msg);
	};

});