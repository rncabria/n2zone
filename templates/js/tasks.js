$(document).ready(function() {
	$('#id-project').on('change', function() {
		var id = $(this).val();
		$.post(process + 'get-members', {id : id}).done(function(data) {
			var json = $.parseJSON(data);
			$('#members').html('');
			$('#members').append('<option value=""></option>');
			$.each(json,function() {
				$('#members').append('<option value="'+this.id_user+'">'+this.username+'</option>');
			});	
			
		});
		
	});
	
	$('.complete-task').on('click', function() {
		var id = $(this).data('id');
		
		if(confirm('Are you sure you want to complete this task? ' + $(this).data('label'))) {
			
			$.post(process + 'complete-task', {id : id}, function (data) {
				if(data) {
					toastr.success('','Task completed!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to mark task as completed.');
				}
			});
				
		}
	});
	
	$('.undo-complete-task').on('click', function() {
		var id = $(this).data('id');
		
		if(confirm('Are you sure you want to mark task as incomplete? ' + $(this).data('label'))) {
			
			$.post(process + 'undo-complete-task', {id : id}, function (data) {
				if(data) {
					toastr.success('','Undo task completed successful!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to mark task as incomplete.');
				}
			});
				
		}
	});	
});