$(document).ready(function() {
	if($('.datepicker').length > 0) $('.datepicker').datepicker();
        
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();	

	if($('#calendar').length)
	$('#calendar').fullCalendar({
		 header: {
			  center: 'title',
			  left : ''
		 },
		draggable : false,
		droppable : false,
		editable: false,
		dayClick : function(date) {
			//Show Todays events
			date = new Date(date);
			console.log(date.format("MMM D, YYYY"));
			$('#event-date').html(date.format());
			$('#viewModal').modal('show');			
		 },
		eventClick: function(event, jsEvent, view) {
			$('#event-list').html('<h3>' + event.title + '</h3>');
			if(event.description) $('#event-list').append('<p>' + event.description + '</p>');
			if(event.url) $('#event-list').append('<a href="'+event.url+'">' + event.url + '</a>');
			
			var start = event.start;
			
			$('#event-date').html(start.format("MMM D, YYYY"));
			if(event.end) $('#event-date').append(' - ' + event.end.format("MMM D, YYYY"));
			  $(this).css('background-color', 'red');
			  $('#viewModal').modal('show');
				if (event.url) {
					//window.open(event.url);
					return false;
				}			  
		 },
		events : $.parseJSON($('#events').html()),
	});
	

	$('#calendar-btn').on('click',function() {
		var btn = $(this);
		var form = $('#form');
		 console.log(form.serialize());
		 
		if(form.valid()) {
			$('.form-footer btn').hide();
			$('.sk-spinner').show();				
				
			var action =  'save';
			
			btn.prop('disabled', true);
			$('.alert').remove();
			$.post(process + action, form.serialize(), function (data) {
				if(data) {
					toastr.success('','Save successful!');
					$('#category-modal').modal('hide');
					setTimeout(function() {
						location.reload();
						
					}, 1500);

				} else {
					toastr.error('','Error saving to database');
				}
			});
			btn.prop('disabled', false);
			$('.form-footer btn').show();
			$('.sk-spinner').hide();
			
		}
		return false;

	});	
});