$(document).ready(function(){

	Dropzone.options.myAwesomeDropzone = {

		autoProcessQueue: false,
		uploadMultiple: true,
		parallelUploads: 10,
		maxFiles: 10,

		// Dropzone settings
		init: function() {
			var myDropzone = this;

			this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
				e.preventDefault();
				e.stopPropagation();
				
				console.log(this);
				
				myDropzone.processQueue();
			});
			this.on("sendingmultiple", function() {
			});
			this.on("successmultiple", function(files, response) {
				toastr.success('','Files successfully uploaded!');
				setTimeout(function() {
					location.reload();
				}, 1500);
			});
			this.on("errormultiple", function(files, response) {
			});
		},
		sending : function(file, xhr, formData) {
			formData.append("data[id_project]", $("#form_id_project").val());
			formData.append("data[id_map]", $("#form_id_map").val());
			formData.append("data[is_public]", $("#form_public").val());
		}

	}

	$(document).on('click','.edit-file', function() {
		$('#edit-id').val($(this).data('id'));
		$('#edit-name').html($(this).data('label'));
		$('#edit-project').val($(this).data('project')).change();
		$('#edit-marker').val($(this).data('marker'));
		$('#edit-public').val($(this).data('public'));
		$('#update-file-modal').modal('show');

	});

	$('#form_id_project').on('change', function() {
		
		$.post(process + 'get-markers',{id : $(this).val()}).done(function(data){
			var json = $.parseJSON(data);
			$('#form_id_map').html('<option value="">&nbsp;</option>');
			$.each(json, function() {
				$('#form_id_map').append('<option value="'+this.id_map+'">'+this.label+'</option>');
			});
		}); 
	});
	
	$('#edit-project').on('change', function() {
		
		$.post(process + 'get-markers',{id : $(this).val()}).done(function(data){
			var json = $.parseJSON(data);
			$('#edit-marker').html('');
			$.each(json, function() {
				$('#edit-marker').append('<option value="'+this.id_map+'">'+this.label+'</option>');
			});
		}); 
	});	

	$('#update-file-btn').on('click',function() {
		var btn = $(this);
		var form = $('#form');
		 
		if(form.valid()) {
			$('.form-footer btn').hide();
			$('.sk-spinner').show();				
				
			var action =  'save';
			
			btn.prop('disabled', true);
			$('.alert').remove();
			$.post(process + action, form.serialize(), function (data) {
				if(data) {
					toastr.success('','Save successful!');
					$('#update-file-modal').modal('hide');
					setTimeout(function() {
						location.reload();
						
					}, 1500);

				} else {
					toastr.error('','Error saving to database');
				}
			});
			btn.prop('disabled', false);
			$('.form-footer btn').show();
			$('.sk-spinner').hide();
			
		}
		return false;

	});	
	
});