$(document).ready(function() {
	
	$('.resend').on('click', function() {
		var email = $(this).data('email');
		var id = $(this).data('id');
		
		var action = 'resend-confirmation-email';
		if(confirm('Are you sure you want to resend confirmation email? ' + $(this).data('label'))) {
			
			$.post(base + 'dashboard/process/' + action, {email : email, id : id}, function (data) {
				if(data) {
					toastr.success('','Confirmation email successfully sent!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to send confirmation email.');
				}
			});
				
		}
	});		
	
	$('.imgpreview').imgupload2(); 
	
	$('.export').on('click',function() {
		$('#export').val(1);
		$(this).closest('form').submit();
		$('#export').val(0);
	});
	
	$('.pdf').on('click',function() {
		$('#pdf').val(1);
		$(this).closest('form').submit();
		$('#pdf').val(0);
	});	
	
	$('.change-pwd-button').on('click',function() {
		var btn = $(this);
		var form = $('#change_pass_form');
		 
		if(form.find('input').filter(function(){ return $(this).val() == ''; }).length == 0) {		
			$('#change_pass_footer .btn').hide();
			$('#change_pass_footer .sk-spinner').show();				
				
			var action = 'change-pass';			
			
			btn.prop('disabled', true);
			$('.alert').remove();
			$.ajax({
				url: process + action,
				type: 'POST',
				data: new FormData(form[0]),
				async: true,
				success: function (data) {
					var jdata = JSON.parse(data);
					if(jdata.status == 1) {
						toastr.success('',jdata.msg);
						// btn.siblings('button.btn-white').on('click');
						// window.location.href = module_view + data;
					} else 
					if(jdata.status == 0) {
						toastr.error('',jdata.msg);
					}
				},
				cache: false,
				contentType: false,
				processData: false
			});
			btn.prop('disabled', false);
			$('#change_pass_footer .btn').show();
			$('#change_pass_footer .sk-spinner').hide();
			return false;						
		} else {
			return false;
		}
	});	
});