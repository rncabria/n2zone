$(document).ready(function() {

	$('.approve-member').on('click', function() {
		var id = $(this).data('id');
		var user = $(this).data('label');
		if(confirm('Are you sure you want to approve this member request? ' + user)) {
			
			$.post(base + 'projects/process/approve-membership', {id : id}).done( function (data) {
				if(data) {
					toastr.success('', user + ' is now a member of this project!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to approve user request.');
				}
			});
				
		}
	});	

// 	if($("#map-canvas").length > 0) {
// var map = $("#map-canvas"),  menu = new Gmap3Menu(map);
// var markers=[];
// if($('#mapMarkers').html().length > 0) {

// 	var jsonObj = $.parseJSON( $('#mapMarkers').html());
// 	if(jsonObj !== null){
// 		$.each(jsonObj, function(i, item) {
// 			var marker = new Object();
// 			marker.lat = jsonObj[i].latitude;
// 			marker.lng = jsonObj[i].longitude;
// 			marker.data = jsonObj[i].location;
// 			marker.options = new Object();
// 			marker.options.icon = new google.maps.MarkerImage("http://maps.google.com/mapfiles/marker.png");
// 			markers.push(marker);
// 		});
// 	}
// }

//        // INITIALIZE GOOGLE MAP
//         map.gmap3({
// 			getlatlng:{
// 				callback: function(results){
// 					if ( !results ) return;
// 					$(this).gmap3({
// 						marker:{
// 							values : markers,
// 							options: {draggable: false}
// 						}
// 					});
// 				}
// 			},
// 			map:{
// 				options:{
// 					center:new google.maps.LatLng(12.3667, 123.9000),
// 					zoom:5,
// 					minZoom : 3,
// 				},
// 			},
// 			marker:{
// 				values:markers,
// 				options: {draggable: false}
// 			}
//         });	 
// 	}

	$('#member-btn').on('click',function() {
		var btn = $(this);
		var form = $('#form-' + btn.data('action'));
		 console.log(form.serialize());
		 
		if(form.valid()) {		
			$('.form-footer btn').hide();
			$('.sk-spinner').show();				
				
			var action =  'save-' + btn.data('action') || 'save';			
			
			btn.prop('disabled', true);
			$('.alert').remove();
			$.post(process + action, form.serialize(), function (data) {
				if(data) {
					toastr.success('','Save successful!');
					$('#member-modal').modal('hide');
					setTimeout(function() {
						location.reload();
						
					}, 1500);

				} else {
					toastr.error('','Error saving to database');
				}
			});
			btn.prop('disabled', false);
			$('.form-footer btn').show();
			$('.sk-spinner').hide();
			
		}
		return false;

	});
	
	$('.edit').on('click',function() {
		$('#pm-role').val($(this).data('role'));
		$('#pm-id').val($(this).data('id'));
		$('#pm-user').val($(this).data('user'));
		$('#pm-user').prop('disabled', true);
		$('.modal-title').html('Edit Member');
		$('#member-modal').modal('show');
	});

	$('.add').on('click', function() {
		$('#member-modal form select').val('');
		$('#pm-id').val(0);
		$('#pm-user').prop('disabled', false);
		$('.modal-title').html('Add Member');
		
	});
	
	$('#request-member-btn').on('click',function() {
		var btn = $(this);
		var form = $('#form-' + btn.data('action'));
		 console.log(form.serialize());
		 
		if(form.valid()) {		
			$('.form-footer btn').hide();
			$('.sk-spinner').show();				

			btn.prop('disabled', true);
			$('.alert').remove();
			$.post(process + btn.data('action'), form.serialize(), function (data) {
				if(data) {
					toastr.success('','Save successful!');
					$('#member-modal').modal('hide');
					setTimeout(function() {
						location.reload();
						
					}, 1500);

				} else {
					toastr.error('','Error saving to database');
				}
			});
			btn.prop('disabled', false);
			$('.form-footer btn').show();
			$('.sk-spinner').hide();
			
		}
		return false;

	});
	
	$('.request').on('click',function() {
		$('#pr-id').val($(this).data('id'));
		$('#pr-name').html($(this).data('label'));
		$('#request-member-modal').modal('show');
		
	});
	
	$('.cancel').on('click', function() {
		var id = $(this).data('id');
		var action = 'cancel-membership';
		if(confirm('Are you sure you want to cancel membership to this project? ' + $(this).data('label'))) {
			
			$.post(process + action, {id : id}, function (data) {
				if(data) {
					toastr.success('','Membership Request successfully canceled!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to cancel project membership.');
				}
			});
				
		}
	});

	$('.change-status').on('click',  function() {
		var id = $(this).data('id');
		var status = $(this).data('status');
		var action = 'change-status';
		if(confirm('Are you sure you want to '+ (status == 1 ? 'approve' : 'cancel') +' this project? ' + $(this).data('label'))) {
			
			$.post(process + action, {id : id, status : status}, function (data) {
				if(data) {
					toastr.success('','Project successfully created!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to cancel project.');
				}
			});
				
		}
		
	});

	if($('.datepicker').length > 0) $('.datepicker').datepicker();


	$('#calendar-btn').on('click',function() {
		var btn = $(this);
		var form = $('#form');
		 console.log(form.serialize());
		 
		if(form.valid()) {
			$('.form-footer btn').hide();
			$('.sk-spinner').show();				
				
			var action =  'save-calendar';
			
			btn.prop('disabled', true);
			$('.alert').remove();
			$.post(process + action, form.serialize(), function (data) {
				if(data) {
					toastr.success('','Save successful!');
					$('#category-modal').modal('hide');
					setTimeout(function() {
						location.reload();
						
					}, 1500);

				} else {
					toastr.error('','Error saving to database');
				}
			});
			btn.prop('disabled', false);
			$('.form-footer btn').show();
			$('.sk-spinner').hide();
			
		}
		return false;

	});	

	$('#category-btn').on('click',function() {
		var btn = $(this);
		var form = $('#form-' + btn.data('action'));
		 console.log(form.serialize());
		 
		if(form.valid()) {		
			$('.form-footer btn').hide();
			$('.sk-spinner').show();				
				
			var action =  'save-' + btn.data('action') || 'save';			
			
			btn.prop('disabled', true);
			$('.alert').remove();
			$.post(process + action, form.serialize(), function (data) {
				if(data) {
					toastr.success('','Save successful!');
					$('#category-modal').modal('hide');
					setTimeout(function() {
						location.reload();
						
					}, 1500);

				} else {
					toastr.error('','Error saving to database');
				}
			});
			btn.prop('disabled', false);
			$('.form-footer btn').show();
			$('.sk-spinner').hide();
			
		}
		return false;

	});
	$('.cat-edit').on('click',function() {
		$('#m-id').val($(this).data('id'));
		$('#m-code').val($(this).data('code'));
		$('#m-name').val($(this).data('name'));
		$('#m-description').val($(this).data('description'));
		$('.modal-title').html('Edit Category');
		$('#category-modal').modal('show');
	});
	$('.cat-add').on('click', function() {
		$('.modal-title').html('Add Category');
		
	});		

});