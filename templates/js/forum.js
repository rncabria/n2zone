$(document).ready(function() {
	$('#category-btn').on('click',function() {
		var btn = $(this);
		var form = $('#form-' + btn.data('action'));
		 console.log(form.serialize());
		 
		if(form.valid()) {		
			$('.form-footer btn').hide();
			$('.sk-spinner').show();				
				
			var action =  'save-' + btn.data('action') || 'save';			
			
			btn.prop('disabled', true);
			$('.alert').remove();
			$.post(process + action, form.serialize(), function (data) {
				if(data) {
					toastr.success('','Save successful!');
					$('#category-modal').modal('hide');
					setTimeout(function() {
						location.reload();
						
					}, 1500);

				} else {
					toastr.error('','Error saving to database');
				}
			});
			btn.prop('disabled', false);
			$('.form-footer btn').show();
			$('.sk-spinner').hide();
			
		}
		return false;

	});
	$('.edit').on('click',function() {
		$('#m-id').val($(this).data('id'));
		$('#m-code').val($(this).data('code'));
		$('#m-name').val($(this).data('name'));
		$('#m-description').val($(this).data('description'));
		
		if($('#m-comment').length > 0) $('#m-comment').html($(this).closest('.media').find('.media-body').html());
		else $('.modal-title').html('Edit Category');
		$('#category-modal').modal('show');
		//console.log($(this).closest('.media').find('.media-body').html());

	});
	$('.add').on('click', function() {
		$('.modal-title').html('Add Category');
		
	});	
});
