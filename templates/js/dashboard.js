$(document).ready(function() {
	if($("#map-canvas").length > 0) {
var map = $("#map-canvas"),  menu = new Gmap3Menu(map);
var markers=[];

if($('#mapMarkers').html().length > 0) {
	var jsonObj = $.parseJSON( $('#mapMarkers').html());
	if(jsonObj !== null){
		$.each(jsonObj, function(i, item) {
			var marker = new Object();
			marker.lat = jsonObj[i].latitude;
			marker.lng = jsonObj[i].longitude;
			marker.data = jsonObj[i].location;
			marker.options = new Object();
			marker.options.icon = new google.maps.MarkerImage("http://maps.google.com/mapfiles/marker.png");
			markers.push(marker);
		});
	}
}

       // INITIALIZE GOOGLE MAP
        map.gmap3({
			getlatlng:{
				callback: function(results){
					if ( !results ) return;
					$(this).gmap3({
						marker:{
							values : markers,
							options: {draggable: false}
						}
					});
				}
			},
			map:{
				options:{
					center:new google.maps.LatLng(12.3667, 123.9000),
					zoom:5,
					minZoom : 3,
				},
			},
			marker:{
				values:markers,
				options: {draggable: false}
			}
        });	 
	}


	$('.cancel-membership').on('click', function() {
		var id = $(this).data('id');
		if(confirm('Are you sure you want to cancel membership to this project? ' + $(this).data('label'))) {
			
			$.post(base + 'projects/process/cancel-membership' , {id : id}, function (data) {
				if(data) {
					toastr.success('','Membership Request successfully canceled!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to cancel project membership.');
				}
			});
				
		}
	});

	$('.delete-project').on('click', function() {
		var id = $(this).data('id');

		if(confirm('Are you sure you want to delete this project? ' + $(this).data('label'))) {
			
			$.post(base + 'projects/process/delete', {id : id}, function (data) {
				if(data) {
					toastr.success('','Project successfully deleted!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to delete project.');
				}
			});
				
		}
	});	

	$('.approve-project').on('click', function() {
		var id = $(this).data('id');

		if(confirm('Are you sure you want to approve this project request? ' + $(this).data('label'))) {
			
			$.post(base + 'projects/process/change-status', {id : id, status : 1} , function (data) {
				if(data) {
					toastr.success('','Project successfully approved!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to approve project.');
				}
			});
				
		}
	});	

	$('.approve-user').on('click', function() {
		var id = $(this).data('id');

		if(confirm('Are you sure you want to approve this user account? ' + $(this).data('label'))) {
			
			$.post(base + 'users/process/activate', {id : id, status : 1}, function (data) {
				if(data) {
					toastr.success('','User is now active!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to approve user account request');
				}
			});
				
		}
	});	

	$('.delete-user').on('click', function() {
		var id = $(this).data('id');

		if(confirm('Are you sure you want to delete this user? ' + $(this).data('label'))) {
			
			$.post(base + 'users/process/delete', {id : id}, function (data) {
				if(data) {
					toastr.success('','User successfully deleted!');
					setTimeout(function() {
						location.reload();
					}, 1500);
				} else {
					toastr.error('','Something went wrong. Unable to delete user.');
				}
			});
				
		}
	});				
});