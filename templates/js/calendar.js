$(document).ready(function() {
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();	

	if($('#calendar').length)
	$('#calendar').fullCalendar({
		 header: {
			  center: 'title',
			  left : ''
		 },
		draggable : false,
		droppable : false,
		editable: false,
		dayClick : function(date) {
			//Show Todays events
			date = new Date(date);
			console.log(date.format("MMM D, YYYY"));
			$('#event-date').html(date.format());
			$('#viewModal').modal('show');			
		 },
		eventClick: function(event, jsEvent, view) {
			$('#event-list').html('<h3>' + event.title + '</h3>');
			if(event.description) $('#event-list').append('<p>' + event.description + '</p>');
			if(event.url) $('#event-list').append('<a href="'+event.url+'">' + event.url + '</a>');
			
			var start = event.start;
			console.log(event.start);
			$('#event-date').html(start.format("MMMM D, YYYY"));
			
			  $(this).css('background-color', 'red');
			  $('#viewModal').modal('show');
				if (event.url) {
					//window.open(event.url);
					return false;
				}			  
		 },
		events : $.parseJSON($('#events').html()),
	});
	
});