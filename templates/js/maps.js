$(document).ready(function() {
       var map = $("#map-canvas"), 
          menu = new Gmap3Menu(map),          
          current;  // current click event (used to save as start / end position)

    function addMarker(){
		
          // add marker and store it
          map.gmap3({
            marker:{
              latLng: current.latLng,
			  content : current.latLng.toString(),
              events: {
                rightclick : function(marker){
                  if(confirm('Are you sure you want to delete this marker?')) {
						// $.post(process + 'delete', {id : id}, function (data) {
							// if(data) {
								// toastr.success('','Delete Successful!');
								// setTimeout(function() {
									// location.reload();
								// }, 1500);
							// } else {
								// toastr.error('','Something went wrong. Unable to delete item.');
							// }
						// });
				  }
                }, 
				click :
					function() {
						window.location.href = module_view + data;
					}
				
              },
            }
          });
		  
        }	

	$('#add-marker-btn').on('click',function() {
	
		var btn = $(this);
		var form = $('form');
		 
		if(form.valid()) {		
			$('.form-footer btn').hide();
			$('.sk-spinner').show();				
				
			var action = btn.data('action') || 'save';			
			
			btn.prop('disabled', true);
			$('.alert').remove();
			$.post(process + action, form.serialize(), function (data) {
				if(data) {
					toastr.success('','Save successful!');
					addMarker(current.latLng);
					$('#add-marker-modal').modal('hide');

				} else {
					toastr.error('','Error saving to database');
				}
			});
			btn.prop('disabled', false);
			$('.form-footer btn').show();
			$('.sk-spinner').hide();
			
		}
		return false;

	});

        // MENU : ITEM 1
        menu.add("Add Marker", "addMarker", 
          function(){
            menu.close();
			console.log(current);
			$('#latitude').val(current.latLng.G);
			$('#longitude').val(current.latLng.K);
			$('input[name="data[label]"]').val('');
			$('textarea[name="data[description]"]').val('');

			$('#add-marker-modal').modal('show');
            
          });

        
        // MENU : ITEM 3
        menu.add("Zoom in", "zoomIn", 
          function(){
            var map = map.gmap3("get");
            map.setZoom(map.getZoom() + 1);
            menu.close();
          });
        
        // MENU : ITEM 4
        menu.add("Zoom out", "zoomOut",
          function(){
            var map = map.gmap3("get");
            map.setZoom(map.getZoom() - 1);
            menu.close();
          });
        
        // MENU : ITEM 5
        menu.add("Center here", "centerHere", 
          function(){
              map.gmap3("get").setCenter(current.latLng);
              menu.close();
          });
        
 
		var markers=[];
		var jsonObj = $.parseJSON( $('#mapMarkers').html());
		if(jsonObj !== null){
			$.each(jsonObj, function(i, item) {
				var marker = new Object();
				marker.lat = jsonObj[i].latitude;
				marker.lng = jsonObj[i].longitude;
				marker.data = jsonObj[i].label;
				marker.options = new Object();
				marker.options.icon = new google.maps.MarkerImage("http://maps.google.com/mapfiles/marker.png");
				markers.push(marker);
			});
		}
		//console.log(markers);

       // INITIALIZE GOOGLE MAP
        map.gmap3({
			getlatlng:{
				callback: function(results){
					if ( !results ) return;
					$(this).gmap3({
						marker:{
							values : markers,
							options: {draggable: false}
						}
					});
				}
			},
			map:{
				options:{
					center:new google.maps.LatLng(12.3667, 123.9000),
					zoom:5,
					minZoom : 3,
					mapTypeId: google.maps.MapTypeId.TERRAIN,
					mapTypeControl: true,
				     mapTypeControlOptions: {
				       style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
				     },
				     navigationControl: true,
				     scrollwheel: true,
				     streetViewControl: true					
				},
				events:{
					rightclick:function(map, event){
					current = event;
					console.log(event);
					menu.open(current);
					},
					click: function(){
						menu.close();
					},
					dragstart: function(){
						menu.close();
					},
					zoom_changed: function(){
						menu.close();
					}
				}
			},
			marker:{
				values:markers,
				options: {draggable: false}
			}
        });	 		
		
		
});